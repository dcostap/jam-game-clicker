package com.pixel.rdf.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.tools.texturepacker.TexturePacker
import com.dcostap.engine_2d.engine.Engine
import com.pixel.rdf.ThisGame

import java.io.*
import java.util.Scanner

object SimulatorLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        ThisGame.SIMULATE = true

        DesktopLauncher.main(arg)
    }
}

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()

        ThisGame.initStaticVars()

        config.width = (Engine.BASE_WIDTH * 1.0).toInt()
        config.height = (Engine.BASE_HEIGHT * 1.0).toInt()
        config.vSyncEnabled = false
        config.foregroundFPS = 60
        config.backgroundFPS = 60
        config.title = "libgdx"

        checkWhetherToPackImages()

        LwjglApplication(ThisGame(), config)
    }

    var exportAtlasFolder = "atlas"
    var imagesOrigin = "../../workingAssets\\atlas\\textures_finished"
    var hashFileName = "textureHash.txt"

    /** Will use texturePacker to pack all images, only if files have changed or atlas output files are not created
     * Loads settings from pack.json files in folders
     * Gradle task texturePacker does the same packing, Android launcher won't pack so use the task if needed  */
    fun checkWhetherToPackImages() {
        println("_____\nChecking image hashes...")
        var initTime = System.currentTimeMillis()

        val hashFile = File(imagesOrigin + "/" + hashFileName)

        val hashingTotal = hashAllFiles(File(imagesOrigin), hashFile)
        var c = 0
        try {
            val atlasFile = File(exportAtlasFolder + "/atlas.atlas")
            val atlasImageFile = File(exportAtlasFolder + "/atlas.png")
            if (!hashFile.exists() || !atlasFile.exists() || !atlasImageFile.exists()) {
                val pw = PrintWriter(hashFile)
                pw.print(-1)
                pw.close()
            }
            val s = Scanner(hashFile)
            c = s.nextInt()
            if (hashingTotal != c) {
                val pw = PrintWriter(hashFile)
                pw.print(hashingTotal)
                pw.close()
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        println("Finished; time elapsed: " + java.lang.Double.toString((System.currentTimeMillis() - initTime) / 1000.toDouble()) + " s\n_____\n")

        if (hashingTotal != c) {
            initTime = System.currentTimeMillis()
            println("_____\nPacking images...")
            packImages(imagesOrigin)
            println("Finished; time elapsed: " + java.lang.Double.toString((System.currentTimeMillis() - initTime) / 1000.toDouble()) + " s\n_____\n")
        }
    }

    private fun hashAllFiles(file: File, hashFile: File): Int {
        var total = 0
        if (file.isDirectory) {
            for (content in file.listFiles()) {
                total += hashAllFiles(content, hashFile)
            }
        } else {
            if (file == hashFile) {
                return 0
            }

            val input = FileInputStream(file.path)
            var c = 0
            while (c != -1) {
                c = input.read()
                total += c
            }
        }
        return total
    }

    private fun packImages(imagesFolder: String) {
        TexturePacker.process(imagesFolder, "atlas", "atlas")
    }
}

