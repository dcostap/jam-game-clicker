package com.pixel.rdf.game_screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.utils.*
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.engine_2d.engine.utils.*
import com.dcostap.engine_2d.engine.utils.Timer
import com.dcostap.engine_2d.engine.utils.screens.BaseScreenWithHud
import com.pixel.rdf.ThisGame
import java.util.*
import java.io.*
import ktx.collections.GdxArray
import ktx.collections.toGdxArray

/**
 * Created by Darius on 30/03/2018.
 */
class GameScreen(val game: ThisGame, dontLoadPreferences: Boolean, hasTutorials: Boolean = true) : BaseScreenWithHud(game) {
    var money = 0.0
    var totalProducedRubberDucks = 0
    var cashPerRubberDuck = 0.75
    val cashPerDuckUpgrade = object : Generator(this) {
        init {
            priceGrowth.basePrice = 45.0
            priceGrowth.multiplier = 2.1
            priceGrowth.powerExpMult = 1.0
            priceGrowth.growFast = 170.0
            prodIncrease = 0.23
            prodInitial = 0.23
        }

        override fun buyOne() {
            super.buyOne()

            cashPerRubberDuck += prodIncrease
        }

        override fun forSimulationGetDescription(): String {
            return "Upgrade cash per duck; cost: ${getPrice()}; cashPerDuck: $cashPerRubberDuck"
        }

        override fun forSimulationGetImprovementMoneyPerSecond(gs: GameScreen): Double {
            var totalProduction = 0.0
            for (factory in factories) {
                if (factory is GameScreen.BuyableFactory) continue
                totalProduction += Math.min(factory.getTruckProductivity(), factory.getGeneratorProductivity())
            }
            var current = totalProduction * cashPerRubberDuck
            val value = (totalProduction * (cashPerRubberDuck + prodIncrease) - current)
            print("CashPerRubberDuck upgrade improvement: $value")
            return value
        }
    }

    val initialDucksPerTouch = 1
    var ducksPerTouch = initialDucksPerTouch
    var globalMultiplier = 1f

    var statDucksPerSecond = 0.0
    var statMoneyPerSecond = 0.0

    var gameLoopPaused = false

    internal var savingLoop = Timer(0.5f)

    var genMult = kotlin.Array<Double>(4, {1.0})
    var truckMult = kotlin.Array<Double>(4, {1.0})

    var allowedGen = arrayOf(true, false, false, false)

    var allowedTruck = arrayOf(true, false, false)

    var ducksPerTouchPercentOfProduction = 0.0

    private val gameResearches = GameResearches(this)
    val researches = gameResearches.researches

    open class Generator(val gameScreen: GameScreen, val description: String = "") : Buyable {
        var numberReductionForPricing: Int = 0
        var prodIncrease = 0.0
        var prodInitial = 0.0
        var number = 0
        var prodMultiplier = 0.0

        val priceGrowth = ExponentialGrowth()

        fun getCost(producerNumber: Int = number): Double {
            return priceGrowth.getValue(Math.max(producerNumber - numberReductionForPricing, 0))
        }

        override fun forSimulationGetDescription(): String {
            return description
        }

        override fun getPrice(): Double {
            return getCost()
        }

        fun getProductivity(producerNumber: Int = number): Double {
            return Math.max(if (producerNumber == 1) prodInitial * getTotalMultiplier() else
                if (producerNumber == 0) 0.0 else ((producerNumber - 1) * prodIncrease + prodInitial) * getTotalMultiplier(), 0.0)
        }

        private fun getTotalMultiplier(): Double = prodMultiplier + gameScreen.globalMultiplier

        override fun buyOne() {
            number++
        }

        override fun forSimulationGetImprovementMoneyPerSecond(gs: GameScreen): Double {
            var isTruck = false

            for (factory in gs.factories) {
                if (factory is GameScreen.BuyableFactory) continue
                if (factory.trucks.contains(this)) {
                    isTruck = true
                    break
                }
            }

            var globalMoneyProd = 0.0
            var totalTrucksProd = 0.0
            var totalProd = 0.0

            for (factory in gs.factories) {
                if (factory is BuyableFactory) continue
                globalMoneyProd += Math.min(factory.getGeneratorProductivity(), factory.getTruckProductivity())
                totalTrucksProd += factory.getTruckProductivity()
                totalProd += factory.getGeneratorProductivity()
            }
            globalMoneyProd *= gs.cashPerRubberDuck

            var index = 0
            var multiplier = 0.0
            for (factory in gs.factories) {
                if (!isTruck) {
                    factory.generators.forEachIndexed { i, gen ->
                        if (gen === this) {
                            index = i
                            return@forEachIndexed
                        }
                    }
                    multiplier = gs.genMult[index]
                } else {
                    factory.trucks.forEachIndexed { i, gen ->
                        if (gen === this) {
                            index = i
                            return@forEachIndexed
                        }
                    }
                    multiplier = gs.truckMult[index]
                }
            }

            var improvement = this.prodIncrease * multiplier
            if (!isTruck && (totalProd + improvement) > totalTrucksProd)
                improvement = Math.min((totalProd + improvement) - totalTrucksProd, improvement)

//            println("improvement: $improvement cost: $cost")

            improvement *= gs.cashPerRubberDuck

            println("GENERATOR upgrade improvement: $improvement\n\t\tthisProdIncrease:${prodIncrease * multiplier}; totalProd:$totalProd; " +
                    "totalTrucksProd:$totalTrucksProd")
//            println("GEN IMPROVEMENT: $improvement")
            return improvement
        }
    }

    val factories = Array<Factory>()
    val factoryCost = Generator(this).also {
        it.prodInitial = 0.0
        it.prodIncrease = it.prodInitial
        it.priceGrowth.basePrice = 8500.0
        it.priceGrowth.multiplier = 1.6
        it.priceGrowth.growFast = 2100000.0
        it.priceGrowth.powerExpMult = 1.8
        it.priceGrowth.initialValues = doubleArrayOf(85000.0, 200000.0, 600000.0, 1500000.0)
    }
    var currentFactory: Factory = Factory(this, 0)

    class BuyableFactory(gameScreen: GameScreen) : Factory(gameScreen, 0)

    fun buyOneFactory() {
        money -= factoryCost.getCost()
        factoryCost.buyOne()

        factories.pop()
        factories.add(Factory(this, factories.size).also {
            changeCurrentFactory(it)
        })
        factories.add(BuyableFactory(this))
    }

    init {
        factories.add(currentFactory)
        factories.add(BuyableFactory(this))
    }

    fun changeCurrentFactory(newFactory: Factory) {
        currentFactory = newFactory

        if (mainUI.currentTab === mainUI.factoryTab) {
            mainUI.factoryTab.redoUI()
        }
    }

    val mainUI = MainUI(this, uiController)

    val savingVersion = "1.2"

    val fadeInterpolator = Interpolator(Interpolation.pow2, 1f, 0f, 0f)
    var fadeIn = false
        set(value) {
            if (value) {
                fadeInterpolator.endValue = 0f
                fadeInterpolator.startValue = 1f
                fadeInterpolator.resetElapsed()
            }
            field = value
        }

    var fadeOut = false
        set(value) {
            if (value) {
                fadeInterpolator.endValue = 1f
                fadeInterpolator.startValue = 0f
                fadeInterpolator.resetElapsed()
            }
            field = value
        }

    var updateTimes = 1

    private var rubberDuckProducingAccumulator = 0.0
    private var rubberDuckSellingAccumulator = 0.0

    private var statDucksPerSecondAcc = 0.0
    private var statMoneyPerSecondAcc = 0.0
    private val statTimer = Timer(1f)

    var availResearch = 0

    init {
        fadeIn = true

        if (!dontLoadPreferences) loadPreferences()

        if (!hasTutorials) {
//            mainUI.tutFactoryDone = true
//            mainUI.firstExpansionPointDone = true
//            mainUI.marketingButtonEnabled = true
//            mainUI.researchButtonEnabled = true
//            mainUI.marketingTab.firstTimeEnteringMarketingTab = false
//            mainUI.factoryTab.factoryTabEnable = true
//            mainUI.marketingTab.finishedAnimateUpgradingTab = true
//            mainUI.marketingTab.animateUpgradingTab = false
        }

        mainUI.createUI()

        savePreferences()

        if (ThisGame.SIMULATE) {
            simulate()
        }
    }

    fun simulate() {
        for (i in 0 until allowedGen.size) {
            allowedGen[i] = true
        }
        for (i in 0 until allowedTruck.size) {
            allowedTruck[i] = true
        }
        val simulationSeconds = 60 * 60
        val pythonFolder = "C:\\Users\\Darius\\Mis documentos\\PROJECTS stuff\\Java projects\\LIBGDX_projects" +
                "\\first-jam-clicker-v2\\workingAssets\\plotly"

        val seconds = Array<Double>()
        val moneyHist = Array<Double>()
        val totalDucksHist = Array<Double>()
        val prodHist = Array<Double>()
        val sellHist = Array<Double>()
        val upgradesHist = Array<String>()
        val researchesHist = ObjectMap<String, Array<Double>>()
        for (research in researches) {
            researchesHist.put(formatNumber(research.cost) + "$;" + research.description, Array())
        }
        val factPriceHist = Array<Double>()

        class FactoryHist {
            val genNumberHist = ObjectMap<Int, Array<Double>>()
            val truckNumberHist = ObjectMap<Int, Array<Double>>()
            val genCostHist = ObjectMap<Int, Array<Double>>()
            val truckCostHist = ObjectMap<Int, Array<Double>>()
            init {
                for (i in 0..10) {
                    genNumberHist.put(i, Array<Double>())
                    truckNumberHist.put(i, Array<Double>())
                    genCostHist.put(i, Array<Double>())
                    truckCostHist.put(i, Array<Double>())
                }
            }

            val prodHist = Array<Double>()
            val sellHist = Array<Double>()
            val unsoldLimitHist = Array<Double>()
            val unsoldLimitCost = Array<Double>()
        }
        val factoriesHist = Array<FactoryHist>()
        factoriesHist.add(FactoryHist())

        fun getWorth(cost: Double, improvementMoneyPerSecond: Double): Double {
            var improvement = improvementMoneyPerSecond
            var globalMoneyProd = 0.0
            var totalTrucksProd = 0.0

            for (factory in factories) {
                if (factory is BuyableFactory) continue
                globalMoneyProd += Math.min(factory.getGeneratorProductivity(), factory.getTruckProductivity())
                totalTrucksProd += factory.getTruckProductivity()
            }
            globalMoneyProd *= cashPerRubberDuck

            if (improvementMoneyPerSecond <= 0)
                improvement = 0.00000001
            if (globalMoneyProd <= 0)
                globalMoneyProd = 0.0000001

            return (cost / globalMoneyProd) + (cost / (improvement))
        }

        var objectiveBuy: Buyable? = null
        var objectiveName: String? = null

        fun smooth(number: Double): Double {
            var n = number * 100
            val nInt = Math.round(n)
            n = nInt.toDouble() / 100.0
            return n
        }

        class DummyFactoryUpgrade : Buyable {
            override fun buyOne() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun getPrice(): Double {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun forSimulationGetImprovementMoneyPerSecond(gs: GameScreen): Double {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun forSimulationGetDescription(): String {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        var debug: Buyable? = null
        fun debugPrint(string: String = "") {
            if (debug == null) return
//            println("--- $string\n--- hash: ${debug!!.javaClass.hashCode()} current number now: ${debug!!.number}; current price now: ${debug!!.getPrice()}")
        }

        for (i in 0..simulationSeconds) {
            var upgradeHistString = ""
            currentFactory.extraProducedDucks += if (i < 30) 4 else 1
            currentFactory.extraSoldDucks += if (i < 15) 5 else 2

            val factoriesCopy = GdxArray(factories)
            if (objectiveBuy == null) {
                if (statMoneyPerSecond * 50f > factoryCost.getPrice()) { // BUY FACTORY?
                    objectiveBuy = DummyFactoryUpgrade()
                } else {
                    var minWorth = -1.0
                    for (factory in factoriesCopy) {
                        if (factory is BuyableFactory) {
                            continue
                        }
                        val allGens = GdxArray<Generator>()
                        allGens.addAll(factory.generators.toGdxArray())
                        if (factory.getTruckProductivity() < factory.getGeneratorProductivity())
                            allGens.addAll(factory.trucks.toGdxArray())
                        for (generator in allGens) {
                            val worth = getWorth(generator.getPrice(), generator.forSimulationGetImprovementMoneyPerSecond(this))
//                            println("----- GEMERATOR: RANDOM\n\t worth: $worth")
                            if (worth < minWorth || minWorth == -1.0) {
                                minWorth = worth
                                objectiveBuy = generator
                                objectiveName = if (factory.generators.contains(generator)) "generator"
                                else "truck"
//                            println((if (objectiveName == "truck") "TRUCK!!!!" else "") + "new best option: minWorth: $minWorth")
                            }
//                        debugPrint("After checking one generator on the factory\n")
                        }
                        val unsoldLimitUpgr = getWorth(factory.unsoldLimitUpgrade.getCost(),
                                factory.unsoldLimitUpgrade.forSimulationGetImprovementMoneyPerSecond(this))
                        if (unsoldLimitUpgr < minWorth) {
                            minWorth = unsoldLimitUpgr
                            objectiveBuy = factory.unsoldLimitUpgrade
                        }
//                    if (factory.)
//                    debugPrint("After checking one factory\n")
                    }
                    val duckPriceWorth = getWorth(cashPerDuckUpgrade.getPrice(), cashPerDuckUpgrade.forSimulationGetImprovementMoneyPerSecond(this))
                    if (duckPriceWorth < minWorth || minWorth == -1.0) {
                        objectiveBuy = cashPerDuckUpgrade
                    }
                    for (research in researches) {
                        if (!research.isAvailable()) continue
                        val worth = getWorth(research.getPrice(), research.forSimulationGetImprovementMoneyPerSecond(this))
//                        println("----- RESEARCH: ${research.description}\n\t worth: $worth")
                        if (worth < minWorth) {
                            minWorth = worth
                            objectiveBuy = research
                        }
                    }
                    println("----- DECIDED TO BUY: ${objectiveBuy!!.forSimulationGetDescription()}; minworth: $minWorth")
//                debugPrint("After checking duckPrice factory\n")
//                println("is debug the same as obj? " + (debug === objectiveBuy).toString())
//                println("Decided to buy new gen; hash: ${objectiveBuy!!.javaClass.hashCode()} \nprice: ${objectiveBuy!!.getPrice()}; number already bought: ${objectiveBuy.number}")
//                debugPrint("AFTER PRINTING THE THING BEFORE\n")
                }
            } else {
                if (objectiveBuy is DummyFactoryUpgrade) {
//                    println("TRYNA BUY FACTORY")
                    if (money >= factoryCost.getPrice()) {
                        buyOneFactory()
                        objectiveBuy = null
                    }
                } else if (money >= objectiveBuy.getPrice()) {
                    money -= objectiveBuy.getPrice()
                    upgradeHistString = objectiveBuy.forSimulationGetDescription()
                    objectiveBuy.buyOne()

//                    println("Bought upgrade!!! hash: ${objectiveBuy.javaClass.hashCode()} \ncurrent number now: ${objectiveBuy.number}; current cost now: ${objectiveBuy.getCost()}")
                    debug = objectiveBuy
                    objectiveBuy = null
                }
            }

            gameLoop(1f)

            factories.forEachIndexed { index, factory ->
                if (factory !is BuyableFactory) {
                    if (index > factoriesHist.size - 1) factoriesHist.add(FactoryHist())
                    factory.generators.forEachIndexed { genIndex, generator ->
                        factoriesHist[index].genCostHist[genIndex].add(generator.getPrice())
                        factoriesHist[index].genNumberHist[genIndex].add(generator.number.toDouble())
                    }
                    factory.trucks.forEachIndexed { genIndex, generator ->
                        factoriesHist[index].truckCostHist[genIndex].add(generator.getPrice())
                        factoriesHist[index].truckNumberHist[genIndex].add(generator.number.toDouble())
                    }
                    factoriesHist[index].prodHist.add(factory.ducksProducedStep)
                    factoriesHist[index].sellHist.add(factory.getTruckProductivity())
                    factoriesHist[index].unsoldLimitHist.add(factory.unsoldLimit)
                    factoriesHist[index].unsoldLimitCost.add(factory.unsoldLimitUpgrade.getCost())
                }
            }

//            debugPrint("After buying and doing loop:")
//            println("factory truck prod: " + factories.first().getTruckProductivity())

            upgradesHist.add(upgradeHistString)
            seconds.add(smooth(i.toDouble()))
            moneyHist.add(smooth(money))
            factPriceHist.add(smooth(factoryCost.getCost()))
            prodHist.add(smooth(statDucksPerSecond))
            sellHist.add(smooth(statMoneyPerSecond))
            totalDucksHist.add(smooth(totalProducedRubberDucks.toDouble()))

            for (research in researches) {
                researchesHist.get(formatNumber(research.cost) + "$;" + research.description).add(
                        if (research.isAvailable()) research.ducksNeeded.toDouble()
                        else 0.0
                )
            }
        }

        fun jsonArray(iterable: Iterable<Double>): JsonValue {
            val json = JsonValue(JsonValue.ValueType.array)
            for (i in iterable) {
                json.addChild(JsonValue(i))
            }
            return json
        }

        fun jsonArray(iterable: Iterable<String>): JsonValue {
            val json = JsonValue(JsonValue.ValueType.array)
            for (i in iterable) {
                json.addChild(JsonValue(i))
            }
            return json
        }

        val jsonObject = JsonValue(JsonValue.ValueType.`object`)

        jsonObject.addChild("seconds", jsonArray(seconds))
        jsonObject.addChild("money", jsonArray(moneyHist))
        jsonObject.addChild("factPrice", jsonArray(factPriceHist))
        jsonObject.addChild("sell", jsonArray(sellHist))
        jsonObject.addChild("prod", jsonArray(prodHist))
        jsonObject.addChild("totalDucks", jsonArray(totalDucksHist))
        jsonObject.addChild("upgrades", jsonArray(upgradesHist))
        factoriesHist.forEachIndexed { index, factoryHist ->
            jsonObject.addChild("factory$index", JsonValue(JsonValue.ValueType.`object`).also {
                it.addChild("prod", jsonArray(factoryHist.prodHist))
                it.addChild("sell", jsonArray(factoryHist.sellHist))
                it.addChild("unsold", jsonArray(factoryHist.unsoldLimitHist))
                it.addChild("unsoldCost", jsonArray(factoryHist.unsoldLimitCost))
                factoryHist.genCostHist.forEachIndexed { index, generator ->
                    it.addChild("gencost$index", jsonArray(generator.value))
                }
                factoryHist.genNumberHist.forEachIndexed { index, generator ->
                    it.addChild("genNumber$index", jsonArray(generator.value))
                }
                factoryHist.truckCostHist.forEachIndexed { index, generator ->
                    it.addChild("truckcost$index", jsonArray(generator.value))
                }
                factoryHist.truckNumberHist.forEachIndexed { index, generator ->
                    it.addChild("trucknumber$index", jsonArray(generator.value))
                }
            })
        }
        jsonObject.addChild("factoryNumber", JsonValue(factoriesHist.size.toDouble()))
        jsonObject.addChild("researches", JsonValue(JsonValue.ValueType.`object`).also {
            for (research in researchesHist) {
                it.addChild(research.key, JsonValue(JsonValue.ValueType.array).also {
                    for (i in research.value) {
                        it.addChild(JsonValue(i))
                    }
                })
            }
        })

        val wr = PrintWriter(pythonFolder + "/data.json")
        val json = jsonObject.toJson(JsonWriter.OutputType.json)
        wr.write(json)
        wr.close()

        Runtime.getRuntime().exec("python ${pythonFolder}/kotlinSimulator.py")

        Gdx.app.exit()
    }

    override fun update(delta: Float) {
        super.update(delta)
        mainUI.update(delta)

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            Gdx.app.exit()
        }

        //gameMap.update(delta);
        gameLoop(delta)

        if (savingLoop.tick(delta)) {
            savePreferences()
        }

        val previousAvailResearch = availResearch
        availResearch = 0
        for (research in researches) {
            if (research.isAvailable()) {
                availResearch++
            }
        }

        if (availResearch != previousAvailResearch) mainUI.marketingTab.updateResearchesOnScreen()
    }

    fun resetGame() {
        Gdx.app.getPreferences("gdx_rdf").clear()
        Gdx.app.getPreferences("gdx_rdf").flush()
        game.restartGameScreen()
    }

    fun getGlobalMultiplierByCurrentFactoryStats(): Float {
        return 1 + totalProducedRubberDucks / 700_000f
    }
    fun resetGameWithPrestige() {
        Gdx.app.getPreferences("gdx_rdf").clear()
        Gdx.app.getPreferences("gdx_rdf").flush()
        game.restartGameScreen(getGlobalMultiplierByCurrentFactoryStats(), false)
    }

    fun getDecimalPart(number: Double): Double {
        return number - number.toInt()
    }

    private fun gameLoop(delta: Float) {
        if (gameLoopPaused) return

        for (i in 0 until updateTimes) {
            var ducksMade = 0.0
            var ducksSold = 0.0
            for (factory in factories) {
                factory.update(delta)
                ducksMade += factory.ducksProducedStep
                ducksSold += factory.ducksSoldStep
            }

            rubberDuckSellingAccumulator += ducksSold
            statMoneyPerSecondAcc += ducksSold

            val ducksSoldInt = Math.floor(rubberDuckSellingAccumulator).toInt()
            rubberDuckSellingAccumulator = getDecimalPart(rubberDuckSellingAccumulator)

            // rubber ducks producing
            rubberDuckProducingAccumulator += ducksMade
            statDucksPerSecondAcc += ducksMade

            val ducksMadeInt = Math.floor(rubberDuckProducingAccumulator).toInt()
            rubberDuckProducingAccumulator = getDecimalPart(rubberDuckProducingAccumulator)

            totalProducedRubberDucks += ducksMadeInt

            if (ducksSoldInt > 0) {
                money += ducksSoldInt * cashPerRubberDuck
            }

            if (statTimer.tick(delta)) {
                statDucksPerSecond = statDucksPerSecondAcc
                statDucksPerSecondAcc = 0.0

                // calculate ducks per touch bonuses based on total production
                ducksPerTouch = MathUtils.floor((initialDucksPerTouch + statDucksPerSecond * ducksPerTouchPercentOfProduction).toFloat())

                statMoneyPerSecond = statMoneyPerSecondAcc
                statMoneyPerSecondAcc = 0.0
            }
        }
    }

    private val backColor = Utils.getColorFrom255RGB(225, 217, 197, 1f)

    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        // clear screen
        Gdx.gl.glClearColor(backColor.r, backColor.g, backColor.b, backColor.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        uiController.draw(gameDrawer, delta)

        if (fadeIn || fadeOut) {
            // update camera & viewport
            viewport.apply(true)

            // start the batch
            game.batch.setProjectionMatrix(camera.combined)
            game.batch.begin()

            fadeInterpolator.update(delta)
            gameDrawer.color = Color.BLACK
            gameDrawer.alpha = fadeInterpolator.getValue()
            gameDrawer.drawRectangle(0f, 0f, percentOfAppWidth(100f), percentOfAppHeight(100f), 0f, true)

            if (fadeInterpolator.hasFinished) {
                fadeIn = false
                fadeOut = false
            }

            game.batch.end()
        }

    }

    override fun createViewport(): Viewport {
        return ScreenViewport(camera)
    }

    override fun touchDownEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, button: Int, pointer: Int, isJustPressed: Boolean) {

    }

    override fun touchReleasedEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, button: Int, pointer: Int) {

    }

    //region preferences
    private fun loadPreferences() {
        val preferences = Gdx.app.getPreferences("gdx_rdf")

        if (preferences.getString("savingVersion", savingVersion) != savingVersion) {
            return
        }

    }

    fun savePreferences() {
        val preferences = Gdx.app.getPreferences("gdx_rdf")

        preferences.putString("savingVersion", savingVersion)


        preferences.flush()
    }
    //endregion

    fun formatMoney(money: Double): String {
        return formatNumber(money) + " $"
    }

    fun formatNumber(number: Int): String {
        return if (number < 1_000_000) {
            String.format(Locale.US, "%,d", number)
        } else {
            String.format(Locale.US, "%.3f", (number / 1000000f)) + "M"
        }
    }

    fun formatNumber(number: Double): String {
        return if (number < 1000000) {
            String.format(Locale.US, "%,.2f", number)
        } else {
            String.format(Locale.US, "%,.3f", number / 1000000f) + "M"
        }
    }
}