package com.pixel.rdf.game_screen

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.dcostap.engine_2d.engine.ui.UIController
import com.dcostap.engine_2d.engine.ui.UICreator
import com.dcostap.engine_2d.engine.ui.utils.ExtImage
import com.dcostap.engine_2d.engine.ui.utils.ExtLabel
import com.dcostap.engine_2d.engine.utils.percentOfAppHeight
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth
import com.dcostap.engine_2d.engine.utils.whenNotNull
import com.pixel.rdf.defaultButtonPadding
import ktx.collections.GdxArray

/**
 * Created by Darius on 30/03/2018.
 */
class MarketingTab(val mainUI: MainUI, uiController: UIController) : UICreator(uiController) {
    val gs = mainUI.gameScreen
    val assets = gs.game.assets

    class ResearchOnScreen(val table: Table, val research: Research) {var movingToY = -1f}

    val researchesOnScreen = GdxArray<ResearchOnScreen>()
    var tabScrollPaneTable: Table? = null
    var tabScrollPane: ScrollPane? = null
    var scrollPaneLastScrollY = 0f

    var referenceResearchesXPos = 0f

    override fun createUI() {
        // if tab exists from before, store its previous scroll position to keep it after creating the new UI
        tabScrollPane.whenNotNull {
            scrollPaneLastScrollY = it.scrollY
            println(it.scrollY.toString())
        }

        mainUI.tabsTable.clearChildren()

        // previous researches on the array to make animation for the new ones
        val previousResearches = GdxArray<Research>()
        for (research in researchesOnScreen) {
            previousResearches.add(research.research)
        }

        researchesOnScreen.clear()

        mainUI.addBaseScrollPaneTableForTabs().let {
            tabScrollPaneTable = it
            tabScrollPane = it.parent as ScrollPane
            it.add(mainUI.tableWithShadowRectangle().also {
                it.defaults().center().fillX()
                it.add(ExtImage(mainUI.duckImage).also {
                    it.scaleToWidth(percentOfAppWidth(11f))
                })
                it.add(ExtLabel("selling price:", assets.fontDefault.font_big)).padLeft(percentOfAppWidth(3f))
                it.add(ExtLabel("", assets.fontDefault.font_medium, mainUI.moneyColor).also {
                    it.setTextUpdateFunction { gs.formatMoney(gs.cashPerRubberDuck) }
                }).padLeft(percentOfAppWidth(3f))
            })
            it.row()

            it.add(mainUI.tableWithShadowRectangle().also {
                it.defaults().fillX()
                it.add(Table().also {
                    it.add(ExtLabel("Improve Marketing:", assets.fontDefault.font_medium)).left()
                    it.row()
                    it.add(ExtLabel("", assets.fontNotOutline.font_medium, Color.BLACK).also {
                        it.setTextUpdateFunction { "+ ${gs.formatMoney(gs.cashPerDuckUpgrade.prodIncrease)} to duck's \nselling price" }
                    }).left()
                }).left().fillX()

                it.add(CustomButton(skin).also {
                    it.defaultButtonPadding()
                    it.add(ExtLabel("", assets.fontDefault.font_medium, mainUI.moneyColor).also {
                        it.setTextUpdateFunction { mainUI.gameScreen.formatMoney(gs.cashPerDuckUpgrade.getPrice()) }
                    })
                    it.enableCondition = { mainUI.gameScreen.money >= gs.cashPerDuckUpgrade.getPrice() }
                    it.addChangeListener {
                        mainUI.gameScreen.money -= gs.cashPerDuckUpgrade.getPrice()
                        gs.cashPerDuckUpgrade.buyOne()
                    }
                }).right().minWidth(mainUI.minButtonWidth).fillX()
            }).padTop(percentOfAppHeight(1.25f)).expandX().fillX().padBottom(percentOfAppHeight(2.3f))

            it.row()

            for (research in gs.researches) {
                addResearch(research, it).whenNotNull {
                    if (!previousResearches.contains(research)) {
                        it.setScale(0f)
                        it.isTransform = true
                        it.addAction(actionForNewResearchesAppearence())
                    }
                }
            }

            tabScrollPane!!.layout()
            tabScrollPane!!.scrollY = scrollPaneLastScrollY
            tabScrollPane!!.updateVisualScroll()
        }
    }

    val researchTableHeight = percentOfAppHeight(10f)
    val researchTableTopPad = percentOfAppHeight(1.25f)

    /** Adds one research table with research info */
    fun addResearch(research: Research, table: Table): Table? {
        if (!research.isAvailable()) return null

        var addedTable: Table? = null
        table.add(mainUI.tableWithShadowRectangle().also {
            researchesOnScreen.add(ResearchOnScreen(it, research))

            addedTable = it
            it.defaults().fillX()
            it.add(Table().also {
                it.add(ExtLabel(research.description, assets.fontNotOutline.font_medium, Color.BLACK).also {
                    it.setWrap(true)
                }).left()
                        .width(percentOfAppWidth(60f))
            }).left().fillX()

            it.add(CustomButton(skin).also {
                it.defaultButtonPadding()
                it.add(ExtLabel(gs.formatMoney(research.cost), assets.fontDefault.font_medium, mainUI.moneyColor))
                it.enableCondition = { mainUI.gameScreen.money >= research.cost && research.isAvailable() }
                it.addChangeListener {
                    research.buyOne()
                    mainUI.gameScreen.money -= research.cost
                    updateResearchesOnScreen()
                }
            }).right().minWidth(mainUI.minButtonWidth).fillX()
        }).padTop(researchTableTopPad).expandX().fillX().height(researchTableHeight)
        table.row()

        addedTable!!.pack()
        referenceResearchesXPos = addedTable!!.x

        return addedTable
    }

    /** Call when researches might have changed and UI's already been created, to make animations for the changes*/
    fun updateResearchesOnScreen() {
//        println("happened")
        if (mainUI.currentTab != this) return

        // if there are new researches, redo the UI
        val currentResearches = GdxArray<Research>()
        for (researchOnScreen in researchesOnScreen) {
            currentResearches.add(researchOnScreen.research)
        }
        for (research in gs.researches) {
            if (research.isAvailable()) {
                if (!currentResearches.contains(research, true)) {
                    val scrollY = tabScrollPane!!.scrollY
                    createUI()
                    tabScrollPane!!.layout()
                    tabScrollPane!!.scrollY = scrollY
                    tabScrollPane!!.updateVisualScroll()
                    tabScrollPane!!.scrollTo(0f, 0f, 0f, 0f)
//                    tabScrollPaneTable!!.pack()
//                    researchesOnScreen.last().table.addAction(Actions.sequence(
//                            Utils.actionShake(percentOfAppWidth(1f), researchesOnScreen.last().table.x,
//                                    researchesOnScreen.last().table.y)))
                    return
                }
            }
        }

        // remove new unavailable researches
        if (researchesOnScreen.size == 0) return

        val copyResearchesOnScreen = GdxArray<ResearchOnScreen>(researchesOnScreen)
        val topY = researchesOnScreen.first().table.y

        copyResearchesOnScreen.forEachIndexed { index, researchOnScreen ->
            if (!researchOnScreen.research.isAvailable()) {
                var newIndex = index
                researchOnScreen.table.isTransform = true
                researchesOnScreen.removeValue(researchOnScreen, true)

                researchOnScreen.table.clearActions()
                researchOnScreen.table.addAction(Actions.sequence(
                        Actions.parallel(
                                Actions.scaleTo(0f, 0f, 0.35f, Interpolation.pow2),
                                Actions.fadeOut(0.32f, Interpolation.pow2)
                        ),
                        Actions.run {
                            // animation for the researches below: move them upwards
                            researchOnScreen.table.isVisible = false
                            while (newIndex < researchesOnScreen.size) {
                                val newResearch = researchesOnScreen[newIndex]
                                newResearch.table.clearActions()
                                newResearch.table.addAction(Actions.moveTo(newResearch.table.x,
                                        topY - (researchTableHeight + researchTableTopPad) * newIndex,
                                        0.3f, Interpolation.pow2))
                                newIndex += 1
                            }
                            //researchOnScreen.table.remove()
                        }
                ))
            }
        }

//        // make animation for new researches
//        val newResearches = GdxArray<Research>()
//        val currentResearches = GdxArray<Research>()
//        for (researchOnScreen in researchesOnScreen) {
//            currentResearches.add(researchOnScreen.research)
//        }
//        for (research in gs.researches) {
//            if (research.isAvailable()) {
//                if (!currentResearches.contains(research, true)) {
//                    newResearches.add(research)
//                }
//            }
//        }
//
////        tabScrollPaneTable!!.row()
//        for (research in newResearches) {
//            println("ty:" + lastTableY)
//            val table = Table().also {
//                it.setFillParent(true)
//                uiController.stage.addActor(it)
//                it.isTransform = true
//            }
//            addResearch(research, table).whenNotNull {
//                it.pack()
//                it.y = percentOfAppHeight(100f) - lastTableY
//                it.x = referenceResearchesXPos
//                lastTableY += it.height
//                it.isTransform = true
//                it.setScale(0f)
//                it.addAction(actionForNewResearchesAppearence())
//            }
//        }
    }

    fun actionForNewResearchesAppearence(): Action = Actions.sequence(Actions.scaleTo(1f, 1f, 0.55f, Interpolation.swingOut))

    override fun update(delta: Float) {

    }
}