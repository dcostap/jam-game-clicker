from producer import Producer
import plotly
import plotly.graph_objs as go
import math
from plotly import tools

class Factory:
	def __init__(self, genPriceMult, genProdMult, truckPriceMult, truckProdMult, genCostMultMult, seconds = 0, number = 0):
		self.gen1 = Producer("gen1", 10 * genPriceMult[0].getPrice(number), 1.13 * genCostMultMult[0].getCost(number), 0.6 * genProdMult.getCost(number))
		self.generator2 = Producer("gen2", 145 * genPriceMult[1].getPrice(number), 1.14 * genCostMultMult[1].getCost(number), 2 * genProdMult.getCost(number))
		self.generator3 = Producer("gen3", 3100 * genPriceMult[2].getPrice(number), 1.14 * genCostMultMult[2].getCost(number), 18 * genProdMult.getCost(number))
		self.generator4 = Producer("gen4", 28000 * genPriceMult[3].getPrice(number), 1.14 * genCostMultMult[3].getCost(number), 95 * genProdMult.getCost(number))
		
		self.permanentTruckProduction = 12
		self.truck1 = Producer("truck1", 50 * truckPriceMult[0].getPrice(number), 2.45, 28 * truckProdMult.getCost(number))
		self.truck2 = Producer("truck2", 1000 * truckPriceMult[1].getPrice(number), 3, 70 * truckProdMult.getCost(number))
		self.truck3 = Producer("truck3", 32000 * truckPriceMult[2].getPrice(number), 3, 200 * truckProdMult.getCost(number))
		
		self.duckGenerators = [self.gen1, self.generator2, self.generator3, self.generator4]
		self.truckGenerators = [self.truck1, self.truck2, self.truck3]
		self.allGenerators = self.duckGenerators + self.truckGenerators
		
		self.seconds = seconds
		
		self.eventsHist = []
		self.prodHist = []
		self.truckProdHist = []
		self.duckGeneratorNumberHist = {}
		self.duckGeneratorCostHist = {}
		self.truckNumberHist = {}
		self.truckCostHist = {}
		
		for generator in self.duckGenerators:
			self.duckGeneratorNumberHist[generator.name] = []
			self.duckGeneratorCostHist[generator.name] = []

		for generator in self.truckGenerators:
			self.truckNumberHist[generator.name] = []
			self.truckCostHist[generator.name] = []
		
		self.truckNumberGraph = {}
		self.truckCostGraph = {}
		self.duckGeneratorsNumberGraph = {}
		self.duckGeneratorsCostGraph = {}
		self.prodGraph = []
		self.truckProdGraph = []

	def getBestGenPurchaseInList(self, list, globalMoneyProd, moneyPerDuck):
		bestRate = -1
		bestGen = None
		for generator in list:
			cost = generator.getPrice()
			improvement = (generator.productivity * generator.productivityMult) * moneyPerDuck
			if improvement <= 0: improvement = 0.1
			value = (cost / globalMoneyProd) + (cost / (improvement))
			print("value: {}; bestRate: {}".format(value, bestRate))
			if (bestRate == -1 or value < bestRate):
				bestRate = value
				bestGen = generator
			print("value: " + str(value) + "; improvement: " + str(improvement) + "; cost: " + str(cost))
		return [bestRate, bestGen]

	def getBestGeneratorPurchase(self, globalMoneyProd, moneyPerDuck, allowedTruck2, allowedTruck3, allowedGens):
		truckProd = self.getTruckProductivity()
		sum = 0
		index = 0
		for generator in self.duckGenerators:
			if index != 0 and not allowedGens[index - 1]:
				index += 1
				continue
				
			sum += generator.getProductivity()
			index += 1
		generatorProd = sum
		
		if truckProd < generatorProd:
			indexLimit = 1
			if allowedTruck2:
				indexLimit = 2
			if allowedTruck3:
				indexLimit = 3
			list = []
			for i in range(indexLimit):
				list.append(self.truckGenerators[i])
			return self.getBestGenPurchaseInList(list, globalMoneyProd, moneyPerDuck)

		return self.getBestGenPurchaseInList(self.duckGenerators, globalMoneyProd, moneyPerDuck)
		
	def getTotalProductivity(self):
		sum = 0
		for generator in self.duckGenerators:
			sum += generator.getProductivity()
		
		sum = min(self.getTruckProductivity(), sum)
		return sum

	def getTruckProductivity(self):
		sum = self.permanentTruckProduction
		for truck in self.truckGenerators:
			sum += truck.getProductivity()
		
		return sum
		
	def update(self):
		prod = self.getTotalProductivity()
		
		eventText = ""
		self.eventsHist.append(eventText)

		for generator in self.duckGenerators:
			self.duckGeneratorNumberHist[generator.name].append(generator.number)
			self.duckGeneratorCostHist[generator.name].append(generator.getPrice())
		for generator in self.truckGenerators:
			self.truckNumberHist[generator.name].append(generator.number)
			self.truckCostHist[generator.name].append(generator.getPrice())
		
		self.prodHist.append(self.getTotalProductivity())
		self.truckProdHist.append(self.getTruckProductivity())
		return prod

	def makeGraphs(self, secondsHist):
		addedSeconds = [0 for i in range(self.seconds)]
		self.prodGraph = go.Scatter(
		x = [i for i in secondsHist],
		y = addedSeconds + self.prodHist,
		mode = 'lines',
		name = "ducks/s")
		
		self.truckProdGraph = go.Scatter(
		x = [i for i in secondsHist],
		y = addedSeconds + self.truckProdHist,
		mode = 'lines',
		name = "ducks sold/s")
		
		for generator in self.duckGenerators:
			self.duckGeneratorsNumberGraph[generator.name] = go.Scatter(
				x = secondsHist,
				y = addedSeconds + self.duckGeneratorNumberHist[generator.name],
				mode = 'lines',
				name = 'nª of ' + generator.name,
			)
			self.duckGeneratorsCostGraph[generator.name] = go.Scatter(
				x = secondsHist,
				y = addedSeconds +  self.duckGeneratorCostHist[generator.name],
				mode = 'lines',
				name = 'cost of ' + generator.name,
			)


		for generator in self.truckGenerators:
			self.truckNumberGraph[generator.name] = go.Scatter(
				x = secondsHist,
				y = addedSeconds + self.truckNumberHist[generator.name],
				mode = 'lines',
				name = 'nª of ' + generator.name,
			)
			self.truckCostGraph[generator.name] = go.Scatter(
				x = secondsHist,
				y = addedSeconds + self.truckCostHist[generator.name],
				mode = 'lines',
				name = 'cost of ' + generator.name,
			)