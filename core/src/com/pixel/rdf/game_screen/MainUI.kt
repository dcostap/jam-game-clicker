package com.pixel.rdf.game_screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align
import com.dcostap.engine_2d.engine.ui.UIController
import com.dcostap.engine_2d.engine.ui.UICreator
import com.dcostap.engine_2d.engine.ui.utils.ExtImage
import com.dcostap.engine_2d.engine.ui.utils.ExtLabel
import com.dcostap.engine_2d.engine.ui.utils.ExtTable
import com.dcostap.engine_2d.engine.utils.*
import com.pixel.rdf.ThisGame

/**
 * Created by Darius on 30/03/2018.
 */
class MainUI(val gameScreen: GameScreen, uiController: UIController) : UICreator(uiController) {
    val assets = gameScreen.game.assets

    val duckColor = Utils.getColorFrom255RGB(253, 226, 0)
    val moneyColor = Utils.getColorFrom255RGB(174, 227, 172)
    val ducksLabel: ExtLabel = ExtLabel("", assets.fontBig.font, Color.YELLOW)
    val moneyLabel: ExtLabel = ExtLabel("", assets.fontBig.font, moneyColor)

    val factoryButton = CustomButton(assets.skin, "notRounded")
    val marketingButton = CustomButton(assets.skin, "notRounded")
    val researchButton = CustomButton(assets.skin, "notRounded")
    val optionsButton = CustomButton(assets.skin, "notRounded")

    val factoryTab = FactoryTab(this, uiController)
    val marketingTab = MarketingTab(this, uiController)
    val optionsTab = OptionsTab(this, uiController)
    val researchTab = ResearchTab(this, uiController)

    lateinit var currentTab: UICreator
    val buttonArray = arrayOf(factoryButton, marketingButton, researchButton, optionsButton)

    val disableColor = Utils.getColorFrom255RGB(60, 60, 68, 1f)
    val outColor = Utils.getColorFrom255RGB(116, 125, 128, 1f)
    val inColor = Utils.getColorFrom255RGB(222, 222, 240, 1f)

    val blueColor = Utils.getColorFrom255RGB(210, 218, 255, 1f)

    val duckImage = gameScreen.game.assets.findRegion("duck")
    val worldImage = gameScreen.game.assets.findRegion("world")
    val factoryImage = gameScreen.game.assets.findRegion("factory")
    val scienceImage = gameScreen.game.assets.findRegion("science")
    val arrow = gameScreen.game.assets.findRegion("arrow")
    val gear = gameScreen.game.assets.findRegion("gear")
    val buyableFactoryImage = gameScreen.game.assets.findRegion("buyableFactory")

    val tabsBaseColor = Utils.getColorFrom255RGB(168, 216, 230)

    val tabsTable = Table()

    var marketingButtonEnabled = true
    var researchButtonEnabled = false
    var firstExpansionPointDone = false

    val minButtonWidth = percentOfAppWidth(30f)
    val topPartHeight = percentOfAppHeight(35f)

    /** @param killTime -1 = never kill it
     * @param position -1 left, 0 center, 1 right */
    class HelperPopUp(val text: String, killTime: Float = 7f, position: Int = 0, val mainUI: MainUI, val hideOnOptions: Boolean = true)
        : Table() {
        val killTimer = Timer(killTime, killTime != -1f, true)
        var updateFunction: () -> Unit = {}

        init {
            this.touchable = Touchable.disabled
            this.align(Align.top)

            add(ExtImage(mainUI.arrow).also {
                it.scaleToWidth(percentOfAppWidth(5f))
                it.setOrigin(Align.center)
                it.originY -= percentOfAppHeight(0.5f)
                it.color = Color.BLACK
                it.color.a = 0.5f
                it.rotation = 90f
            }).also {
                if (position == 0)
                    it.center()
                if (position == -1)
                    it.left()
                if (position == 1)
                    it.right()
                it.padLeft(percentOfAppWidth(3f))
                it.padRight(percentOfAppWidth(3f))
            }
            row()
            add(Table().also {
                it.touchable = Touchable.disabled
                it.background = mainUI.assets.skin.getDrawable("transparencyButton")
                it.color.a = 0.5f
                // create subclass to override parent alpha, so the text is opaque
                it.add(object : ExtLabel(text, mainUI.assets.fontDefault.font_medium) {
                    init {
                        setWrap(true)
                    }

                    override fun draw(batch: Batch?, parentAlpha: Float) {
                        super.draw(batch, 1f)
                    }
                }).width(percentOfAppWidth(60f)).pad(percentOfAppWidth(1f))
            })

            isTransform = true
            align(Align.center)
            setScale(0f)
            addAction(Actions.sequence(Actions.scaleTo(1.1f, 1.1f, 0.5f, Interpolation.pow2),
                    Actions.scaleTo(1f, 1f, 0.35f, Interpolation.pow2)))

            pack()
        }

        var killed = false
        fun kill() {
            killed = true
        }

        override fun act(delta: Float) {
            super.act(delta)

            if (hideOnOptions)
                isVisible = mainUI.currentTab != mainUI.optionsTab

            if (killTimer.tick(delta) || killed) {
                if (!hasActions()) {
                    addAction(Actions.sequence(Actions.delay(0.4f),
                            Actions.scaleTo(0f, 0f, 0.25f, Interpolation.pow2),
                            Actions.run {remove()}))
                }
            }

            updateFunction()
        }
    }

    init {
        fun changeTab(newTab: UICreator) {
            if (newTab === currentTab) return

            currentTab = newTab

            newTab.createUI()
        }

        factoryButton.addChangeListener {
            changeTab(factoryTab)
            updateButtonChecked(factoryButton)
        }

        optionsButton.addChangeListener {
                changeTab(optionsTab)
                updateButtonChecked(optionsButton)
        }

        researchButton.addChangeListener {
                if (!researchButton.buttonDisabled) {
                    changeTab(researchTab)
                    updateButtonChecked(researchButton)
                }
        }
        marketingButton.addChangeListener {
            if (!marketingButton.buttonDisabled) {
                changeTab(marketingTab)
                updateButtonChecked(marketingButton)
            }
        }

        currentTab = factoryTab
    }

    fun addBaseScrollPaneTableForTabs(): ExtTable {
        val table = ExtTable()
        table.let {
            it.top()

            val scrollPane = ScrollPane(it, skin)
            tabsTable.add(scrollPane).top().expand().fill()

            scrollPane.setFadeScrollBars(false)
            scrollPane.setScrollbarsOnTop(true)
            scrollPane.setScrollingDisabled(true, false)

            scrollPane.color = tabsBaseColor

            val pad = percentOfAppWidth(4f)
            it.pad(0f, pad, 0f, pad)
        }
        return table
    }

    fun tableWithShadowRectangle(): DrawingWidget {
        val w = DrawingWidget(assets, uiController.stage).also {
            it.drawingFunction = { self ->
                self.gameDrawer.color = Color.BLACK
                self.gameDrawer.alpha = 0.11f * it.color.a
                self.gameDrawer.drawRectangle(-percentOfAppWidth(25f), self.y, percentOfAppWidth(125f),
                        self.height, 0f, true)
                self.gameDrawer.resetColorAndAlpha()
            }
        }
        return w
    }

//    val enableTimer = Timer(1.5f, true, true)
//
//    var tutFactoryDone = false

//    var justCreated = false
//    val simulateActSeconds = Timer(0.1f, true, true)

    override fun update(delta: Float) {
        currentTab.update(delta)

//        if (justCreated) {
//            if (simulateActSeconds.tick(delta)) {
//                justCreated = false
//                var i = 0
//
//                while (i < 50) {
//                    i++
//                    uiController.stage.act(0.05f)
//                }
//            }
//        }
//
//        if (gameScreen.money == 0.0 && !tutFactoryDone) {
//            tutFactoryDone = true
//            uiController.stage.addActor(HelperPopUp("Touch the factory to start making rubber ducks!",
//                    -1f, 0, this).also {
//                it.setPosition(percentOfAppWidth(50f), percentOfAppHeight(65f), Align.top)
//                it.updateFunction = {
//                    if (gameScreen.unsoldDucks > 0 && !it.killed) {
//                        it.kill()
//                        uiController.stage.addActor(Actor().also {
//                            it.addAction(Actions.sequence(Actions.delay(1.2f), Actions.run {
//                                uiController.stage.addActor(HelperPopUp("These are all the unsold ducks your factory " +
//                                        "produced. Touch them to sell one and earn money!", -1f, 1, this).also {
//                                    it.setPosition(percentOfAppWidth(69f), percentOfAppHeight(62f), Align.top)
//                                    it.updateFunction = {
//                                        if (gameScreen.money > 0) {
//                                            it.kill()
//                                        }
//                                    }
//                                })
//
//                                it.remove()
//                            }))
//                        })
//                    }
//                }
//            })
//        }

//        if (marketingButtonEnabled) return
//        if (gameScreen.workers > 0 || gameScreen.money > 15f) {
//            enableTimer.tick(delta)
//
//            if (!enableTimer.isTimerOn) {
//                marketingButtonEnabled = true
//            }
//        }
    }

    override fun createUI() {
        tabsTable.clearChildren()

        uiController.stage.isDebugAll = false

        val duckImageUI = ExtImage(duckImage)
        duckImageUI.scaleToWidth(percentOfAppWidth(13f))

        val expansionWindow = Window("", skin)
        uiController.stage.addActor(Table().also {
            it.setFillParent(true)
            it.top().left()

            it.add(Stack().also {
                // touchable factory
                it.add(OnScreenFactories(this))

                if (ThisGame.DEBUG_INFO) {
                    it.add(Table().also {
                        it.left()
                        it.add(ExtLabel("", assets.fontNotOutline.font_medium, Color.BLACK).also {
                            it.setTextUpdateFunction { "fps:" + Gdx.graphics.framesPerSecond.toString() }
                            it.touchable = Touchable.disabled
                        }).left()
                        it.row()
                        it.add(ExtLabel(ThisGame.gitTagVersion, assets.fontNotOutline.font_small, Color.BLACK).also {
                            it.touchable = Touchable.disabled
                        }).left().fillX()
                    })
                }

                // money and ducks number info
                it.add(Table().also {
                    it.padLeft(percentOfAppWidth(2f))
                    it.left()
                    it.add(Table().also {
                        it.add(duckImageUI).top().left().padRight(percentOfAppWidth(1.5f))
                        it.add(ducksLabel).top().left().fillX()
                        it.row()
                        it.add(ExtLabel("", assets.fontNotOutline.font_medium, Color.BLACK).also {
                            it.setTextUpdateFunction { gameScreen.formatNumber(gameScreen.statDucksPerSecond) }
                        }).top().left().fillX()
                    }).left().expandX().top().expandY()

                    it.row()
                    it.add(Table().also {
                        it.add(ExtLabel("", assets.fontNotOutline.font_medium, Color.BLACK).also {
                            it.setTextUpdateFunction { gameScreen.formatNumber(gameScreen.statMoneyPerSecond) }
                        }).bottom().left().fillX()
                        it.row()
                        it.add(moneyLabel).left().fillX()
                    }).bottom().left().expandX()

                    ducksLabel.updateFunction = {ducksLabel.setText(gameScreen.formatNumber(gameScreen.totalProducedRubberDucks))}
                    moneyLabel.updateFunction = {moneyLabel.setText(gameScreen.formatMoney(gameScreen.money))}
                    ducksLabel.updateFunction(0f)
                    moneyLabel.updateFunction(0f)
                })

            }).top().fillX().expandX().left().height(topPartHeight)
            it.row()
            it.add(tabsTable).expand().fill()

            // bottom buttons
            it.row()
            it.bottom()
            it.add(Table().also {
                it.defaults().height(percentOfAppHeight(14f)).fill().bottom().width(percentOfAppWidth(29f))

                for (button in buttonArray) {
                    if (button === optionsButton) {
                        it.add(button).width(percentOfAppWidth(15f))

                        button.add(ExtImage(gear).also {
                            it.setOrigin(Align.center)
                            it.scaleToWidth(percentOfAppWidth(8f))
                        }).center().fill().padLeft(percentOfAppWidth(1f))
                        continue
                    }

                    // the image and a little world icon to show when expansion points can be expanded
                    it.add(Stack().also {
                        it.add(Table().also {
                            it.add(button).expand().fill()
                        })

                        it.add(Table().also {
                            it.isTransform = true
                            it.top().right().padRight(percentOfAppWidth(2f))
                            it.add(ExtImage(worldImage).also {
                                it.scaleToWidth(percentOfAppWidth(5f))
                                it.isVisible = false
                                val itself = it
//                                it.updateFunction = {
//                                    if (gameScreen.expansionPoints > 0 && (button === marketingButton || button === researchButton)) {
//                                        if (!itself.hasActions()) {
//                                            itself.isVisible = true
//                                            itself.color.a = 0f
//                                            itself.addAction(Actions.sequence(
//                                                    Actions.parallel(
//                                                            Actions.fadeIn(0.2f),
//                                                            Actions.scaleTo(1.2f, 1.2f, 1f, Interpolation.pow2)),
//                                                    Actions.delay(0.4f),
//                                                    Actions.parallel(
//                                                            Actions.sequence(Actions.delay(0.2f), Actions.fadeOut(0.25f)),
//                                                            Actions.scaleTo(0.5f, 0.5f, 1f))))
//                                        }
//                                    } else {
//                                        itself.isVisible = false
//                                    }
//                                }
                            }).top().right()
                        })
                    })

                    val sizeWidth = percentOfAppWidth(17f)

                    if (button === factoryButton) {
                        button.add(Stack().also {
                            it.isTransform = true
                            it.add(ExtImage(factoryImage).also {
                                it.scaleToWidth(sizeWidth)
                            })
                        })
                    } else if (button === marketingButton) {
                        button.add(Stack().also {
                            it.isTransform = true
                            it.add(ExtLabel("$", assets.fontBig.font, fontColor = moneyColor, alignment = Align.center))
                            it.add(ExtImage(factoryImage).also {
                                it.scaleToWidth(percentOfAppWidth(15f))
                                it.isVisible = false
                            })
                        })
                    } else if (button === researchButton) {
                        button.add(ExtImage(scienceImage).also {
                            it.scaleToWidth(percentOfAppWidth(12f))
                        })
                    }

                    if (button !== factoryButton && button != optionsButton) {
                        // if buttons disabled, prepare buttons to have contents not visible and to do action when they get enabled
                        if ((!marketingButtonEnabled && button == marketingButton)
                                || (!researchButtonEnabled && button == researchButton)) {
                            button.buttonDisabled = true
                            for (actor in button.children) {
                                actor.isVisible = false
                            }
                        }

                        button.updateFunction = {
                            if (button.buttonDisabled && ((marketingButtonEnabled && button == marketingButton)
                                    || (researchButtonEnabled && button == researchButton))) {
                                button.buttonDisabled = false
                                for (actor in button.children) {
                                    actor.isVisible = true
                                }

                                // refresh the button colors
                                for (b in buttonArray) {
                                    if (b.isChecked) {
                                        updateButtonChecked(b)
                                        break
                                    }
                                }

                                for (actor in button.children) {
                                    if (actor is Stack)
                                        actor.isTransform = true

                                    actor.setScale(0f)
                                    actor.addAction(Actions.sequence(Actions.scaleTo(1.1f, 1.1f, 0.45f, Interpolation.pow2),
                                            Actions.scaleTo(1f, 1f, 0.5f, Interpolation.pow2)))
                                }
                            }
                        }
                    }
                }
            }).bottom().expandX().fillX()
        })

        currentTab.createUI()
        updateButtonChecked(factoryButton)
    }

    fun updateButtonChecked(newCheckedButton: CustomButton) {
        for (button in buttonArray) {
            button.setProgrammaticChangeEvents(false)
            button.isChecked = false
        }

        newCheckedButton.isChecked = true

        for (button in buttonArray) {
            if (button.buttonDisabled) {
                button.color = disableColor
            } else if (button.isChecked) {
                button.color = inColor
            } else {
                button.color = outColor
            }
        }
    }
}
