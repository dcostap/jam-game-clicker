package com.pixel.rdf

import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Value
import com.dcostap.engine_2d.engine.utils.Interpolator
import com.dcostap.engine_2d.engine.utils.Utils
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth

fun <T : Actor> Cell<T>.padTopBottom(pad: Float): Cell<T> {
    this.padTop(pad)
    return this.padBottom(pad)
}

fun <T : Actor> Cell<T>.padLeftRight(pad: Float): Cell<T> {
    this.padLeft(pad)
    return this.padRight(pad)
}

fun Table.padTopBottom(pad: Float): Table {
    this.padTop(pad)
    return this.padBottom(pad)
}

fun Table.padLeftRight(pad: Float): Table {
    this.padLeft(pad)
    return this.padRight(pad)
}

fun Table.defaultButtonPadding(): Table {
    return this.padTop(percentOfAppWidth(0.4f)).padBottom(percentOfAppWidth(3f))
}

/** Created by Darius on 21/04/2018. */
class Utils {
    companion object {
        fun actionShake(maximumMovingQuantity: Float, originalX: Float = 0f, originalY: Float = 0f): Action {
            fun random(): Float {
                return Utils.getRandomFloatInsideRange(0f, maximumMovingQuantity, true)
            }
            fun shake(): Action {
                return Actions.sequence(
                        Actions.moveBy(random(), random(), 0.03f, Interpolation.pow3Out),
                        Actions.moveTo(originalX, originalY, 0.02f, Interpolation.pow3Out))
            }

            return Actions.sequence(shake(),shake(),shake(),shake())
        }
    }
}