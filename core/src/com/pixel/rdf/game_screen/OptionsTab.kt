package com.pixel.rdf.game_screen

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.utils.Align
import com.dcostap.engine_2d.engine.ui.UIController
import com.dcostap.engine_2d.engine.ui.UICreator
import com.dcostap.engine_2d.engine.ui.utils.ExtLabel
import com.dcostap.engine_2d.engine.ui.utils.ExtScrollPane
import com.dcostap.engine_2d.engine.utils.Utils
import com.dcostap.engine_2d.engine.utils.percentOfAppHeight
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth
import com.pixel.rdf.ThisGame

/**
 * Created by Darius on 30/03/2018.
 */
class OptionsTab(val mainUI: MainUI, uiController: UIController) : UICreator(uiController) {

    override fun createUI() {
        mainUI.tabsTable.clearChildren()

        Table().let {
            it.top()
            it.padTop(percentOfAppHeight(2f))

            val scrollPane = ExtScrollPane(it, skin, true, false,
                    true, false)
            mainUI.tabsTable.add(scrollPane).width(percentOfAppWidth(100f)).expand().fill()

            scrollPane.setFadeScrollBars(false)
            scrollPane.setScrollbarsOnTop(true)

            scrollPane.color = mainUI.tabsBaseColor

            it.add(CustomButton(skin).also {
                it.add(ExtLabel("Reset game", mainUI.assets.fontDefault.font_big))
                it.addChangeListener {
                    mainUI.gameScreen.gameLoopPaused = true
                    val darkeningImage = DrawingWidget(mainUI.assets, uiController.stage)
                    darkeningImage.drawingFunction = {
                        it.gameDrawer.color = Color.BLACK
                        it.gameDrawer.alpha = 0.4f
                        it.gameDrawer.drawRectangle(it.x, it.y, it.width, it.height, 0f, true)
                        it.gameDrawer.resetColorAndAlpha()
                    }

                    darkeningImage.setFillParent(true)
                    uiController.stage.addActor(darkeningImage)
                    Utils.modifyScene2dActorToBlockInputBeneathItself(darkeningImage)

                    uiController.stage.addActor(Table().also {
                        it.setFillParent(true)

                        it.add(Window("", skin).also {
                            it.isMovable = false
                            it.clip = false

                            // contents
                            val confirmWindow = it

                            Utils.modifyScene2dActorToBlockInputBeneathItself(it)

                            it.pad(percentOfAppWidth(5f))

                            it.add(ExtLabel("Reset the game",
                                    mainUI.assets.fontDefault.font_big)).pad(percentOfAppWidth(1f))
                            it.row()
                            it.add(ExtLabel("You will lose all the progress.\nAre you sure?",
                                    mainUI.assets.fontNotOutline.font_big, Color.BLACK, alignment = Align.left)).pad(percentOfAppWidth(2f))
                            it.row()
                            it.add(Table().also {
                                it.defaults().width(percentOfAppWidth(30f))
                                it.add(CustomButton(skin).also {
                                    it.add(ExtLabel("YES", mainUI.assets.fontDefault.font_big))
                                    it.addChangeListener {
                                        uiController.stage.addActor(Table().also {
                                            it.setFillParent(true)

                                            val darkeningImage2 = DrawingWidget(mainUI.assets, uiController.stage)
                                            darkeningImage2.drawingFunction = {
                                                it.gameDrawer.color = Color.BLACK
                                                it.gameDrawer.alpha = 0.5f
                                                it.gameDrawer.drawRectangle(it.x, it.y, it.width, it.height, 0f, true)
                                                it.gameDrawer.resetColorAndAlpha()
                                            }

                                            darkeningImage2.setFillParent(true)
                                            uiController.stage.addActor(darkeningImage2)
                                            Utils.modifyScene2dActorToBlockInputBeneathItself(darkeningImage2)

                                            it.add(Window("", skin).also {
                                                it.pad(percentOfAppWidth(5f))
                                                it.isMovable = false
                                                it.clip = false

                                                val confirmConfirmWindow = it

                                                it.add(ExtLabel("Are you sure you are sure?",
                                                        mainUI.assets.fontDefault.font_big).also {
                                                    it.setWrap(true)
                                                }).pad(percentOfAppHeight(1f)).width(percentOfAppWidth(60f)).padTop(percentOfAppHeight(1f))

                                                it.row()
                                                it.add(ExtLabel("All progress will be lost forever! (That's a long time!)",
                                                        mainUI.assets.fontNotOutline.font_medium, Color.BLACK).also {
                                                    it.setWrap(true)
                                                }).pad(percentOfAppHeight(1f)).width(percentOfAppWidth(60f)).padTop(percentOfAppHeight(1f))

                                                it.row()
                                                it.add(Table().also {
                                                    it.defaults().width(percentOfAppWidth(30f))
                                                    it.add(CustomButton(skin).also {
                                                        it.add(ExtLabel("YES", mainUI.assets.fontDefault.font_medium)).pad(percentOfAppWidth(2f))
                                                        it.addChangeListener {
                                                            mainUI.gameScreen.resetGame()
                                                        }
                                                    }).padRight(percentOfAppWidth(10f))

                                                    it.add(CustomButton(skin).also {
                                                        it.add(ExtLabel("NO", mainUI.assets.fontDefault.font_big)).pad(percentOfAppWidth(2f))
                                                        it.addChangeListener {
                                                            mainUI.gameScreen.gameLoopPaused = false
                                                            confirmWindow.remove()
                                                            darkeningImage.remove()
                                                            darkeningImage2.remove()
                                                            confirmConfirmWindow.remove()
                                                        }
                                                    })
                                                }).padTop(percentOfAppHeight(5f))

                                                it.pack()
                                                // pop-up animation
                                                it.isTransform = true
                                                it.setScale(0f)
                                                it.setOrigin(Align.center)
                                                it.addAction(Actions.scaleTo(1f, 1f, 0.32f, Interpolation.pow2))
                                            })
                                        })
                                    }

                                }).padRight(percentOfAppWidth(25f))

                                it.add(CustomButton(skin).also {
                                    it.add(ExtLabel("NO", mainUI.assets.fontDefault.font_big))
                                    it.addChangeListener {
                                        mainUI.gameScreen.gameLoopPaused = false
                                        confirmWindow.remove()
                                        darkeningImage.remove()
                                    }
                                })
                            }).padTop(percentOfAppHeight(5f))

                            it.pack()
                            // pop-up animation
                            it.isTransform = true
                            it.setScale(0f)
                            it.setOrigin(Align.center)
                            it.addAction(Actions.scaleTo(1f, 1f, 0.32f, Interpolation.pow2))
                        })
                    })
                }
            })

            // debug
            if (ThisGame.DEBUG_CHEATS) {
                it.row()
                it.add(CustomButton(skin).also {
                    it.add(ExtLabel("10 thousand MONEY", mainUI.assets.fontNotOutline.font_medium, Color.BLACK))
                    it.addChangeListener { mainUI.gameScreen.money += 10000 }
                })
                it.row()
//                it.add(CustomButton(skin).also {
//                    it.add(ExtLabel("10 thousand ducks", mainUI.assets.fontNotOutline.font_medium))
//                    it.addChangeListener {
//                        mainUI.gameScreen.totalProducedRubberDucks += 10000
//                        mainUI.gameScreen.unsoldDucks += 10000
//                    }
//                })
//                it.row()
//                it.add(CustomButton(skin).also {
//                    it.add(ExtLabel("1 expansion point", mainUI.assets.fontNotOutline.font_medium))
//                    it.addChangeListener { mainUI.gameScreen.expansionPoints += 1 }
//                })
//                it.row()
                it.add(Table().also {
                    it.add(CustomButton(skin).also {
                        it.add(ExtLabel("-", mainUI.assets.fontBig.font))
                        it.addChangeListener {
                            mainUI.gameScreen.updateTimes -= 1
                            if (mainUI.gameScreen.updateTimes < 1) mainUI.gameScreen.updateTimes = 1
                        }
                    })
                    it.add(ExtLabel("", mainUI.assets.fontNotOutline.font_medium, Color.BLACK).also {
                        it.setTextUpdateFunction { "game loop speed: ${mainUI.gameScreen.updateTimes}" }
                    })
                    it.add(CustomButton(skin).also {
                        it.add(ExtLabel("+", mainUI.assets.fontBig.font))
                        it.addChangeListener {
                            mainUI.gameScreen.updateTimes += 1
                        }
                    })
                })
            }
        }
    }

    override fun update(delta: Float) {

    }
}