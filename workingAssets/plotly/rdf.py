import plotly
import plotly.graph_objs as go
import math
from plotly import tools
from factory import Factory
from producer import Producer
from producer import UpgradeHistClass

ducksPerTouch = 1
touchesPerSecond = 0.5
		
money = 0	
moneyPerDuck = 0.75
totalGeneratedDucks = 0
objectiveGenerator = None

touchesPerSecondAcc = 0

secondsHist = []
moneyHist = []
productionHist = []
truckProductionHist = []
moneyPerSecondHist = []
totalGeneratedDucksHist = []
upgradeBuyingHist = []
factoryPriceHist = []

isUpgrade = False

allowedTruck2 = False
allowedTruck3 = False

allowedGens = [False, False, False]

gen1mult = 1
gen2mult = 1
gen3mult = 1
gen4mult = 1
truck1mult = 1
truck2mult = 1
truck3mult = 1

duckCostUpgrade = Producer("duckCostUpgrade", 45, 2.1, 0.23, True, "duckPrice Upgrade", progressionExp=1, growFast=170)

class Upgrade:
	def __init__(self, cost):
		self.cost = cost
		self.done = False
	
	def buy(self, seconds):
		global upgradeBuyingHist
		upgradeBuyingHist.append(UpgradeHistClass(seconds, "buyUpgr: " + self.__class__.__name__ + "; cost: " + str(self.cost)))
		self.done = True
		
	def getMoneyPerSecondIncrease(self):
		pass


class Gen1Upgrade(Upgrade):
	def __init__(self, cost, increase):
		super(Gen1Upgrade, self).__init__(cost)
		self.increase = increase
	
	def buy(self, seconds):
		global gen1mult
		super(Gen1Upgrade, self).buy(seconds)
		gen1mult += self.increase
		
	def getMoneyPerSecondIncrease(self, moneyPerDuck):
		global gen1mult
		initial = getTotalProductivity()
		gen1mult += self.increase
		updateFactoriesWithMultipliers()
		final = getTotalProductivity()
		gen1mult -= self.increase
		updateFactoriesWithMultipliers()
		return (final - initial) * moneyPerDuck

class Gen2Upgrade(Upgrade):
	def __init__(self, cost, increase):
		super(Gen2Upgrade, self).__init__(cost)
		self.increase = increase
	
	def buy(self, seconds):
		global gen2mult
		super(Gen2Upgrade, self).buy(seconds)
		gen2mult += self.increase
		
	def getMoneyPerSecondIncrease(self, moneyPerDuck):
		global gen2mult
		initial = getTotalProductivity()
		gen2mult += self.increase
		updateFactoriesWithMultipliers()
		final = getTotalProductivity()
		gen2mult -= self.increase
		updateFactoriesWithMultipliers()
		return (final - initial) * moneyPerDuck
		
class Gen3Upgrade(Upgrade):
	def __init__(self, cost, increase):
		super(Gen3Upgrade, self).__init__(cost)
		self.increase = increase
	
	def buy(self, seconds):
		global gen3mult
		super(Gen3Upgrade, self).buy(seconds)
		gen3mult += self.increase
		
	def getMoneyPerSecondIncrease(self, moneyPerDuck):
		global gen3mult
		initial = getTotalProductivity()
		gen3mult += self.increase
		updateFactoriesWithMultipliers()
		final = getTotalProductivity()
		gen3mult -= self.increase
		updateFactoriesWithMultipliers()
		return (final - initial) * moneyPerDuck
		
class Gen4Upgrade(Upgrade):
	def __init__(self, cost, increase):
		super(Gen4Upgrade, self).__init__(cost)
		self.increase = increase
	
	def buy(self, seconds):
		global gen4mult
		super(Gen4Upgrade, self).buy(seconds)
		gen4mult += self.increase
		
	def getMoneyPerSecondIncrease(self, moneyPerDuck):
		global gen4mult
		initial = getTotalProductivity()
		gen4mult += self.increase
		updateFactoriesWithMultipliers()
		final = getTotalProductivity()
		gen4mult -= self.increase
		updateFactoriesWithMultipliers()
		return (final - initial) * moneyPerDuck
		
class Truck1Upgrade(Upgrade):
	def __init__(self, cost, increase):
		super(Truck1Upgrade, self).__init__(cost)
		self.increase = increase
	
	def buy(self, seconds):
		global truck1mult
		super(Truck1Upgrade, self).buy(seconds)
		truck1mult += self.increase
		
	def getMoneyPerSecondIncrease(self, moneyPerDuck):
		global truck1mult
		initial = getTotalProductivity()
		truck1mult += self.increase
		updateFactoriesWithMultipliers()
		final = 0
		for factory in factories:
			final += factory.getTruckProductivity()
			
		truck1mult -= self.increase
		updateFactoriesWithMultipliers()
		return ((final - initial) / 1.2) * moneyPerDuck

class Truck2Upgrade(Upgrade):
	def __init__(self, cost, increase):
		super(Truck2Upgrade, self).__init__(cost)
		self.increase = increase
	
	def buy(self, seconds):
		global truck2mult
		super(Truck2Upgrade, self).buy(seconds)
		truck2mult += self.increase
		
	def getMoneyPerSecondIncrease(self, moneyPerDuck):
		global truck2mult
		initial = getTotalProductivity()
		truck2mult += self.increase
		updateFactoriesWithMultipliers()
		final = 0
		for factory in factories:
			final += factory.getTruckProductivity()
			
		truck2mult -= self.increase
		updateFactoriesWithMultipliers()
		return ((final - initial) / 1.2) * moneyPerDuck

upgrades = []
upgrades.append(Gen1Upgrade(100, 0.25))
upgrades.append(Gen1Upgrade(300, 0.2))
upgrades.append(Gen1Upgrade(1600, 0.25))
upgrades.append(Gen1Upgrade(1200000, 0.15))
upgrades.append(Gen1Upgrade(3000000, 0.15))

upgrades.append(Gen2Upgrade(1000, 0.3))
upgrades.append(Gen2Upgrade(4000, 0.35))
upgrades.append(Gen2Upgrade(1000000, 0.37))
upgrades.append(Gen2Upgrade(4000000, 0.38))

upgrades.append(Gen3Upgrade(10000, 0.3))
upgrades.append(Gen3Upgrade(100000, 0.3))
upgrades.append(Gen3Upgrade(300000, 0.3))
upgrades.append(Gen3Upgrade(10000000, 0.3))

upgrades.append(Gen4Upgrade(100000, 0.3))
upgrades.append(Gen4Upgrade(500000, 0.35))
upgrades.append(Gen4Upgrade(1500000, 0.35))
upgrades.append(Gen4Upgrade(25000000, 0.35))

upgrades.append(Truck1Upgrade(1500, 0.5))
upgrades.append(Truck1Upgrade(4000, 0.5))
upgrades.append(Truck1Upgrade(10000, 0.3))
upgrades.append(Truck2Upgrade(80000, 0.3))
upgrades.append(Truck2Upgrade(200000, 0.3))

def getPossibleUpgrades():
	list = []
	for upgrade in upgrades:
		if totalGeneratedDucks >= upgrade.cost * 2:
			list.append(upgrade)
	
	endlist = []
	for item in list:
		if not item.done:
			endlist.append(item)
	
	# print(str(endlist))
	return endlist

def getTotalProductivity():
	sum = 0
	for factory in factories:
		sum += factory.getTotalProductivity()
		
	return sum
	
buyFactory = False
factoryCost = Producer("factory", 85000, 1.6, 0, True, "BUY FACTORY", progressionExp=1.8, 
growFast=2100000, initialPriceValues=[85000, 200000, 600000, 1500000])
factoryGenCostMult1 = Producer("", 1, 1.3, 0, progressionExp=1.4, growFast=0.3)
factoryGenCostMult2 = Producer("", 1, 1.22, 0, progressionExp=1.4, growFast=0.24)
factoryGenCostMult3 = Producer("", 1, 1.15, 0, progressionExp=1.4, growFast=0.2)
factoryGenCostMult4 = Producer("", 1, 1.12, 0, progressionExp=1.4, growFast=0.15)
factoryGenCostMult = [factoryGenCostMult1, factoryGenCostMult2, factoryGenCostMult3, factoryGenCostMult4]

factoryGenCostMultMult1 = Producer("", 1, 1.3, 0, progressionExp=1.2, growFast=2)
factoryGenCostMultMult2 = Producer("", 1, 1.25, 0, progressionExp=1.2, growFast=1.6)
factoryGenCostMultMult3 = Producer("", 1, 1.22, 0, progressionExp=1.2, growFast=1.4)
factoryGenCostMultMult4 = Producer("", 1, 1.14, 0, progressionExp=1.2, growFast=1.2)
factoryGenCostMultMult = [factoryGenCostMultMult1, factoryGenCostMultMult2, factoryGenCostMultMult3, factoryGenCostMultMult4]

factoryGenProdMult = Producer("", 1, 0, 1.4, progressionExp=1.5, growFast=0.4)

factoryTruckCostMult1 = Producer("", 1, 0, 1.35, progressionExp=1.8, growFast=8.5)
factoryTruckCostMult2 = Producer("", 1, 0, 1.2, progressionExp=1.8, growFast=32.25)
factoryTruckCostMult3 = Producer("", 1, 0, 1.1, progressionExp=1.8, growFast=9.1)
factoryTruckCostMult = [factoryTruckCostMult1, factoryTruckCostMult2, factoryTruckCostMult3]
factoryTruckProdMult = Producer("", 0, 0, 1.35, progressionExp=1.5, growFast=0.4)

factories = []
factories.append(Factory(factoryGenCostMult, factoryGenProdMult,factoryTruckCostMult, factoryTruckProdMult, factoryGenCostMultMult))

def checkGeneratorsBuying(seconds):
	global objectiveGenerator
	global money
	global isUpgrade
	global moneyPerDuck
	global duckCostUpgrade
	global upgradeBuyingHist
	global buyFactory
	
	if objectiveGenerator is None:
		if buyFactory:
			if factoryCost.getPrice() <= money:
				money -= factoryCost.getPrice()
				factoryCost.buyOne(seconds, upgradeBuyingHist)
				
				factories.append(Factory(factoryGenCostMult, factoryGenProdMult,
				factoryTruckCostMult, factoryTruckProdMult, factoryGenCostMultMult, seconds, len(factories)))
				buyFactory = False
				return
		else:
			if getTotalProductivity() * moneyPerDuck * 25 >= factoryCost.getPrice():
				print(getTotalProductivity())
				print(factoryCost.getPrice())
				buyFactory = True
				
		if buyFactory: return
		
		isUpgrade = False
		
		bestRate = -1
		globalMoneyProduction = max(getTotalProductivity() * moneyPerDuck, 1)
		for factory in factories:
			option = factory.getBestGeneratorPurchase(globalMoneyProduction, moneyPerDuck, allowedTruck2, allowedTruck3, allowedGens)
			if option[0] < bestRate or bestRate == -1:
				bestRate = option[0]
				objectiveGenerator = option[1]
		#duckCostUpgrade
		cost = duckCostUpgrade.getPrice()
		improvement = duckCostUpgrade.productivity * max(getTotalProductivity(), 1)
		value = (cost / globalMoneyProduction) + (cost / (improvement))
		print("duckCost: " + str(value) + "; best: " + str(bestRate))
		if value < bestRate:
			objectiveGenerator = duckCostUpgrade
		
		for upgrade in getPossibleUpgrades():
			cost = upgrade.cost
			value = (cost / globalMoneyProduction) + (cost / (max(upgrade.getMoneyPerSecondIncrease(moneyPerDuck), 0.1)))
			# if isinstance(upgrade, Truck1Upgrade):
				# print(str(upgrade.__class__.__name__) + ": value: " + str(value) + "; best: " + str(bestRate) 
				# + "; increase: " + str(upgrade.getMoneyPerSecondIncrease(moneyPerDuck)) + "; cost: " + str(cost) 
				# + ";globalM: " + str(globalMoneyProduction))
			if value < bestRate:
				objectiveGenerator = upgrade
				isUpgrade = True
	elif isUpgrade:
		if objectiveGenerator.cost <= money:
			money -= objectiveGenerator.cost
			objectiveGenerator.buy(seconds)
			objectiveGenerator = None
			checkGeneratorsBuying(seconds)
	elif objectiveGenerator.getPrice() <= money:
		print(str(objectiveGenerator.getPrice()))
		# eventText += "Bought generator " + objectiveGenerator.name
		
		money -= objectiveGenerator.getPrice()
		
		objectiveGenerator.buyOne(seconds, upgradeBuyingHist)
		
		objectiveGenerator = None
		
		checkGeneratorsBuying(seconds)

def updateFactoriesWithMultipliers():
	for factory in factories:
		factory.gen1.productivityMult = gen1mult
		factory.generator2.productivityMult = gen2mult
		factory.generator3.productivityMult = gen3mult
		factory.generator4.productivityMult = gen4mult
		factory.truck1.productivityMult = truck1mult
		factory.truck2.productivityMult = truck2mult
		factory.truck3.productivityMult = truck3mult
		
for seconds in range(60 * 120):
	if totalGeneratedDucks > 200:
		allowedGens[0] = True
	if totalGeneratedDucks > 5000:
		allowedGens[1] = True
	if totalGeneratedDucks > 90000:
		allowedGens[2] = True
		
	if totalGeneratedDucks > 100000:	
		allowedTruck2 = True
	if totalGeneratedDucks > 1000000:	
		allowedTruck3 = True
		
	originalMPD = moneyPerDuck
	moneyPerDuck += duckCostUpgrade.getProductivity()
	prod = 0
	touchesPerSecondAcc += touchesPerSecond

	if seconds < 30:
		touchesPerSecondAcc += 2.5
	
	if (touchesPerSecondAcc >= 1):
		value = math.floor(touchesPerSecondAcc)
		prod += value
		touchesPerSecondAcc -= value
	
	for factory in factories:
		prod += factory.update()
	
	totalGeneratedDucks += prod
	moneyPerSecond = prod * moneyPerDuck
	money += moneyPerSecond
	
	checkGeneratorsBuying(seconds)
		
	secondsHist.append(seconds)
	moneyHist.append(money)
	productionHist.append(getTotalProductivity())
	totalGeneratedDucksHist.append(totalGeneratedDucks)
	moneyPerSecondHist.append(moneyPerSecond)
	moneyPerDuck = originalMPD
	factoryPriceHist.append(factoryCost.getPrice())
	
	updateFactoriesWithMultipliers()
	
# graphs
index = 0
for i in secondsHist:
	secondsHist[index] = i / 60 #seconds to minutes
	index += 1

for factory in factories:
	factory.makeGraphs(secondsHist)
	
moneyGraph = go.Scatter(
    x = secondsHist,
    y = moneyHist,
    mode = 'lines+text',
    name = 'money',
	#text = eventsHist,
	textposition='bottom'
)

productionGraph = go.Scatter(
    x = secondsHist,
    y = productionHist,
    mode = 'lines+text',
    name = 'productiviy ducks / s',
)

moneyPerSecondGraph = go.Scatter(
    x = secondsHist,
    y = moneyPerSecondHist,
    mode = 'lines+text',
    name = 'money/s',
)

totalDucksGraph = go.Scatter(
    x = secondsHist,
    y = totalGeneratedDucksHist,
    mode = 'lines',
    name = 'total generated ducks',
)

factoryPriceGraph = go.Scatter(
    x = secondsHist,
    y = factoryPriceHist,
    mode = 'lines',
    name = 'new factory cost',
)

specs=[]
for i in range(5):
	list = []
	for i in range(len(factories)):
		list.append({})
		
	specs.append(list)
for i in range(6):
	list = [{'colspan': len(factories)}]
	for i in range(len(factories) - 1):
		list.append(None)
	specs.append(list)

fig = tools.make_subplots(rows=11, cols=len(factories), specs = specs)

index = 1
for factory in factories:
	for generator in factory.duckGenerators:
		fig.append_trace(factory.duckGeneratorsNumberGraph[generator.name], 1, index)
		fig.append_trace(factory.duckGeneratorsCostGraph[generator.name], 2, index)

	for generator in factory.truckGenerators:
		fig.append_trace(factory.truckNumberGraph[generator.name], 3, index)
		fig.append_trace(factory.truckCostGraph[generator.name], 4, index)
	fig.append_trace(factory.truckProdGraph, 5, index)
	fig.append_trace(factory.prodGraph,5, index)
	index += 1

posy = 0
for upgrade in upgradeBuyingHist:
	value = math.floor(len(upgrade.message) * (max(productionHist) / 32))
	if upgrade.message == "BUY FACTORY":
		value = max(productionHist)
	fig.append_trace(go.Scatter(
		x = [upgrade.seconds / 60],
		y = [value],
		mode = 'lnes+markers',
		connectgaps = True,
		hoveron = "points+fills",
		name = upgrade.message,
		text = upgrade.message,
		showlegend = False,
		marker = dict(
        size = 8)
	),7, 1)
	posy += 22
	if posy >= 100:
		posy = 0

fig.append_trace(productionGraph, 6, 1)
fig.append_trace(moneyPerSecondGraph, 8, 1)
fig.append_trace(moneyGraph,9, 1)
fig.append_trace(totalDucksGraph, 10, 1)
fig.append_trace(factoryPriceGraph, 11, 1)
fig['layout'].update(height=1600, width=2000)
# fig['layout'].hovermode= 'closest'
# fig['layout'].annotations=[
	# dict(
		# x=upgrade.seconds / 60,
		# y=0,
		# xref='x',
		# yref='y',
		# text=upgrade.message,
		# showarrow=True,
		# arrowhead=7,
		# ax=0,
		# ay=-40
	# ) for upgrade in upgradeBuyingHist
# ]

plotly.offline.plot(fig)