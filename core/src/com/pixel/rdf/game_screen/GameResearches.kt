package com.pixel.rdf.game_screen

/**
 * Created by Darius on 27/04/2018.
 */
class GameResearches(gs: GameScreen) {
    val researches = arrayOf(
    GenMultResearch(gs, 100.0, 0, 0.25, 200),
    GenMultResearch(gs, 300.0, 0, 0.2, 600),
    GenMultResearch(gs, 1600.0, 0, 0.25, 3200),

    GenMultResearch(gs, 1000.0, 1, 0.3, 2000),
    GenMultResearch(gs, 4000.0, 1, 0.35, 8000),
    GenMultResearch(gs, 1000000.0, 1, 0.37, 2000000),

    GenMultResearch(gs, 10000.0, 3, 0.3, 20000),
    GenMultResearch(gs, 100000.0, 3, 0.3, 200000),
    GenMultResearch(gs, 300000.0, 3, 0.3, 600000),

    TruckMultResearch(gs, 1500.0, 0, 0.5, 3000),
    TruckMultResearch(gs, 4000.0, 0, 0.5, 8000),
    TruckMultResearch(gs, 10000.0, 0, 0.3, 20000),

    TruckMultResearch(gs, 80000.0, 1, 0.35, 160000),
    TruckMultResearch(gs, 200000.0, 1, 0.35, 400000),
    TruckMultResearch(gs, 800000.0, 1, 0.35, 1600000),

    DuckPerTouchPercentOfProdResearch(gs, 400.0, 0.15, 500),
    DuckPerTouchPercentOfProdResearch(gs, 1000.0, 0.10, 5000),
    DuckPerTouchPercentOfProdResearch(gs, 8000.0, 0.10, 16000),
    DuckPerTouchPercentOfProdResearch(gs, 60000.0, 0.10, 120000),
    DuckPerTouchPercentOfProdResearch(gs, 700000.0, 0.10, 1400000),
    DuckPerTouchPercentOfProdResearch(gs, 5000000.0, 0.10, 10000000)
    )
}
