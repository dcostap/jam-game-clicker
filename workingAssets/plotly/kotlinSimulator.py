import plotly
import plotly.graph_objs as go
import math
from plotly import tools
import json
		
# secondsHist.append(seconds)
# moneyHist.append(money)
# productionHist.append(getTotalProductivity())
# totalGeneratedDucksHist.append(totalGeneratedDucks)
# moneyPerSecondHist.append(moneyPerSecond)
# moneyPerDuck = originalMPD
# factoryPriceHist.append(factoryCost.getPrice())

secondsHist = []
moneyHist = []
totalDucksHist = []
upgradesHist = []
sellHist = []
prodHist = []
factoriesHist = {}
researches = {}
factPriceHist = []
factoriesNumber = 0
with open('data.json') as jsonFile:
	data = json.load(jsonFile)
	secondsHist = data["seconds"]
	moneyHist = data["money"]
	totalDucksHist = data["totalDucks"]
	upgradesHist = data["upgrades"]
	sellHist = data["sell"]
	prodHist = data["prod"]
	researches = data["researches"]
	factoriesNumber = int(data["factoryNumber"])
	factPriceHist = data["factPrice"]
	for i in range(factoriesNumber):
		val = data["factory{}".format(i)]
		factoriesHist[i] = val
		# array["prod"] = data["prod"]
		# array["sell"] = data["sell"]
		# array["sell"] = data["sell"]
		# factoriesHist.append()


# graphs
index = 0
for i in secondsHist:
	secondsHist[index] = i / 60 #seconds to minutes
	index += 1
	
moneyGraph = go.Scatter(
    x = secondsHist,
    y = moneyHist,
    mode = 'lines+text',
    name = 'money',
	#text = eventsHist,
	textposition='bottom'
)

sellGraph = go.Scatter(
    x = secondsHist,
    y = sellHist,
    mode = 'lines+text',
    name = 'money/s',
	#text = eventsHist,
	textposition='bottom'
)

prodGraph = go.Scatter(
    x = secondsHist,
    y = prodHist,
    mode = 'lines+text',
    name = 'prod / s',
	#text = eventsHist,
	textposition='bottom'
)

totalDucksGraph = go.Scatter(
    x = secondsHist,
    y = totalDucksHist,
    mode = 'lines',
    name = 'total generated ducks',
)

factPriceGraph = go.Scatter(
    x = secondsHist,
    y = factPriceHist,
    mode = 'lines',
    name = 'new factory cost',
)

specs=[]
for i in range(6):
	list = []
	for i in range(factoriesNumber):
		list.append({})
		
	specs.append(list)
for i in range(8):
	list = [{'colspan': factoriesNumber}]
	for i in range(factoriesNumber - 1):
		list.append(None)
	specs.append(list)

fig = tools.make_subplots(rows=14, cols=factoriesNumber, specs = specs)

number = 0
for i in range(factoriesNumber):
	i = int(i)
	val = factoriesHist[i]
	factExtra = []

	if number != 0:
		changes = 0
		changeNumber = factPriceHist[0]
		for second in secondsHist:
			if changes == number:
				break

			second *= 60
			factExtra.append(0)
			price = factPriceHist[int(second)]
			if price != changeNumber:
				changeNumber = price
				changes += 1
	number += 1


	fig.append_trace(go.Scatter (
		x = secondsHist,
		y = factExtra + val["prod"],
		mode = 'lines',
		name = 'productivity'), 1, i + 1)
	fig.append_trace(go.Scatter (
		x = secondsHist,
		y = factExtra + val["sell"],
		mode = 'lines',
		name = 'sellingProd'), 1, i + 1)
	fig.append_trace(go.Scatter (
		x = secondsHist,
		y = factExtra + val["unsold"],
		mode = 'lines',
		name = 'unsoldLimit'), 1, i + 1)
	fig.append_trace(go.Scatter (
		x = secondsHist,
		y = factExtra + val["unsoldCost"],
		mode = 'lines',
		name = 'unsoldCost'), 2, i + 1)
	for a in range(4):
		fig.append_trace(go.Scatter (
			x = secondsHist,
			y = factExtra + val["gencost{}".format(a)],
			mode = 'lines',
			name = 'gen cost {}'.format(a)), 3, i + 1)
		fig.append_trace(go.Scatter (
			x = secondsHist,
			y = factExtra + val["genNumber{}".format(a)],
			mode = 'lines',
			name = 'gen number {}'.format(a)), 4, i + 1)
	for a in range(3):
		fig.append_trace(go.Scatter (
			x = secondsHist,
			y = factExtra + val["truckcost{}".format(a)],
			mode = 'lines',
			name = 'truckcost {}'.format(a)), 5, i + 1)
		fig.append_trace(go.Scatter (
			x = secondsHist,
			y = factExtra + val["trucknumber{}".format(a)],
			mode = 'lines',
			name = 'trucknumber {}'.format(a)), 6, i + 1)

fig.append_trace(moneyGraph, 7, 1)
fig.append_trace(totalDucksGraph, 8, 1)
fig.append_trace(totalDucksGraph, 9, 1)
fig.append_trace(prodGraph, 10, 1)
fig.append_trace(sellGraph, 11, 1)
fig.append_trace(factPriceGraph, 12, 1)
fig.append_trace(totalDucksGraph, 13, 1)
for key, value in researches.items():
	thisSeconds = []
	values = []
	for second in secondsHist:
		if value[int(second * 60)] != 0:
			thisSeconds.append(second)
			values.append(value[int(second * 60)])
	fig.append_trace(go.Scatter (
		x = thisSeconds,
		y = values,
		mode = 'lines',
		connectgaps = True,
		hoveron = "points",
		showlegend = False,
		text = key), 13, 1)

posy = 0
i = 0
upgradev2 = []
for upgrade in upgradesHist:
	if upgrade != "":
		upgradev2.append([i, upgrade])
	i += 1

i = 0
for upgrade in upgradev2:
	posy = len(upgrade[1])
	fig.append_trace(go.Scatter(
		x = [upgrade[0] / 60],
		y = [posy],
		mode = 'lines+markers',
		connectgaps = True,
		hoveron = "points+fills",
		name = upgrade[1],
		text = upgrade[1],
		showlegend = False,
		marker = dict(
        size = 9)
	),14, 1)
	i += 1

fig['layout'].update(height=1600, width=2000)
plotly.offline.plot(fig)