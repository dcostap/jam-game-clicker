package com.pixel.rdf.game_screen

import com.badlogic.gdx.math.Interpolation
import com.dcostap.engine_2d.engine.utils.Timer
import com.dcostap.engine_2d.engine.utils.Updatable
import com.dcostap.engine_2d.engine.utils.Utils

/**
 * Created by Darius on 10/04/2018.
 */
class Interpolator(var interpolation: Interpolation, var duration: Float, var startValue: Float, var endValue: Float) : Updatable {
    private val timer = Timer(10000f)

    fun getValue(): Float {
        val value = interpolation.apply(Utils.mapToRange(timer.elapsed, 0f, duration, 0f, 1f))
        return Utils.mapToRange(value, 0f, 1f, startValue, endValue)
    }

    override fun update(delta: Float) {
        timer.tick(delta)
    }

    fun resetElapsed() {
        timer.resetTimer()
    }

    val hasFinished
        get() = timer.elapsed > duration
}
