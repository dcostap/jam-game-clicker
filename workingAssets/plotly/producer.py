class Producer:
	def __init__(self, name, basePrice, multiplier, productivity, notify = False, message = "bought duck Cost upgrade", initialPriceValues = [], progressionExp = 1, growFast = 0):
		self.name = name
		self.basePrice = basePrice
		self.multiplier = multiplier
		self.productivity = productivity
		self.number = 0
		self.notify = notify
		self.message = message
		self.initialPriceValues = initialPriceValues
		self.productivityMult = 1
		self.progressionExp = progressionExp
		self.growFast = growFast
	
	def buyOne(self, seconds, upgradeBuyingHist):
		if self.notify:
			upgradeBuyingHist.append(UpgradeHistClass(seconds, self.message + "; cost:" + str(self.getPrice())))
		self.number += 1
	
	def getPrice(self, number = -1):
		if number == -1:
			number = self.number
		
		if number + 1 <= len(self.initialPriceValues):
			return self.initialPriceValues[number]
		
		exponent = (number * self.progressionExp)
		multiplierTotal = 0
		if exponent == 0:
			multiplierTotal = 0 
		else:
			multiplierTotal = self.multiplier ** exponent
	
		# print("basePrice: {}; multiplierTotal: {}; number * self.growFast: {}".format(self.basePrice, multiplierTotal, number * self.growFast))
		return self.basePrice + multiplierTotal + number * self.growFast
		
	def getProductivity(self):
		return self.number * self.productivity * self.productivityMult
		
class UpgradeHistClass:
	def __init__(self, seconds, message):
		self.seconds = seconds
		self.message = message