package com.pixel.rdf.game_screen

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Array
import com.dcostap.engine_2d.engine.ui.UIController
import com.dcostap.engine_2d.engine.ui.UICreator
import com.dcostap.engine_2d.engine.ui.utils.ExtImage
import com.dcostap.engine_2d.engine.ui.utils.ExtLabel
import com.dcostap.engine_2d.engine.ui.utils.ExtScrollPane
import com.dcostap.engine_2d.engine.utils.Timer
import com.dcostap.engine_2d.engine.utils.Utils
import com.dcostap.engine_2d.engine.utils.percentOfAppHeight
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth
import com.pixel.rdf.ThisGame

/**
 * Created by Darius on 30/03/2018.
 */
class ResearchTab(val mainUI: MainUI, uiController: UIController) : UICreator(uiController) {
    val gs = mainUI.gameScreen

    override fun createUI() {

    }

    override fun update(delta: Float) {

    }
}