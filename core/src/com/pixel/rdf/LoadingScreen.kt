package com.pixel.rdf

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.engine_2d.engine.ui.utils.ExtLabel

/**
 * Created by Darius on 26/02/2018.
 */
class LoadingScreen() : Screen {
    internal var transitioning = false
    internal var stage: Stage

    init {
        stage = Stage(ExtendViewport(Engine.BASE_WIDTH * 1.65f, Engine.BASE_HEIGHT * 1.65f))

        val loadingFont = BitmapFont(Gdx.files.internal("fonts/bitmap/darenciusPixel.fnt"))
        val loading = ExtLabel("loading...", loadingFont, Color(1f, 1f, 1f, 1f), Align.center)
        val name = ExtLabel("Pixel Imperfect Games", loadingFont, Color(0.6f, 0.75f, 1f, 1f), Align.center)
        stage.addActor(Table().also {
            it.setFillParent(true)
            it.add(name)
            it.row()
            it.add(loading).padTop(50f)
        })
    }

    override fun render(delta: Float) {
        // clear screen
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.act(delta)
        stage.draw()
    }

    override fun show() {

    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun pause() {

    }

    override fun resume() {

    }

    override fun hide() {

    }

    override fun dispose() {
        stage.dispose()
    }
}
