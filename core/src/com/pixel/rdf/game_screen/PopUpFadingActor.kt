package com.pixel.rdf.game_screen

import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.dcostap.engine_2d.engine.utils.Utils
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth

/**
 * Created by Darius on 10/04/2018.
 */
class PopUpFadingActor(dontMoveHorizontally: Boolean = false, val touchableFactory: TouchableFactory) : Table() {

    val horMove = Interpolator(Interpolation.exp5Out, if (dontMoveHorizontally) 0f else 1.1f,
            Utils.getRandomFloatInsideRange(0f, percentOfAppWidth(0.8f))
                    * if (Utils.getChanceToBeTrue(50f)) -1 else 1, 0f)
    val verMove = Interpolator(Interpolation.pow3In, 1.1f, percentOfAppWidth(0.4f), percentOfAppWidth(2.5f))
    val alphaChange = Interpolator(Interpolation.pow2, 1.1f, 1f, 0f)

    var factoryMoved = false
    override fun act(delta: Float) {
        super.act(delta)

        horMove.update(delta)
        verMove.update(delta)
        alphaChange.update(delta)

        color.a = alphaChange.getValue()
        moveBy(horMove.getValue(), verMove.getValue())

        if (alphaChange.hasFinished) {
            remove()
        }

        if (!factoryMoved && touchableFactory.position != TouchableFactory.Position.center) {
            factoryMoved = true
            isTransform = true
            setOrigin(Align.center)
            addAction(Actions.sequence(Actions.scaleTo(0f, 0f, 0.18f, Interpolation.pow2), Actions.run { remove() }))
        }
    }
}