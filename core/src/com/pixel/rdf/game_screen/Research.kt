package com.pixel.rdf.game_screen

/**
 * Created by Darius on 27/02/2018.
 */
abstract class Research(val gameScreen: GameScreen, var cost: Double, var description: String = "", var ducksNeeded: Int = -1)
    : Buyable {
    var researched: Boolean = false
    var buyAction: () -> Unit = {}
    var appearCondition: () -> Boolean = {gameScreen.totalProducedRubberDucks >= ducksNeeded}

    fun isAvailable(): Boolean {
        return !researched && appearCondition()
    }

    override fun buyOne() {
        buyAction()
        researched = true
    }

    override fun getPrice(): Double {
        return cost
    }
}

class GenMultResearch(gameScreen: GameScreen, price: Double,
                      val generatorIndex: Int, val multIncrease: Double, ducksNeeded: Int = -1)
    : Research(gameScreen, price, ducksNeeded = ducksNeeded) {
    init {
        description = "Increase productivity of each ${Factory.getGeneratorName(generatorIndex, false)} " +
                "by ${gameScreen.formatNumber(multIncrease)} ducks produced per second"
        buyAction = {gameScreen.genMult[generatorIndex] += multIncrease}
    }

    override fun forSimulationGetImprovementMoneyPerSecond(gs: GameScreen): Double {
        var improvement = 0.0
        for (factory in gs.factories) {
            if (factory is GameScreen.BuyableFactory) continue
            val totalProd = Math.min(factory.getGeneratorProductivity(), factory.getTruckProductivity())
            improvement += Math.min(
                    (totalProd - factory.generators[generatorIndex].getProductivity()) + (factory.generators[generatorIndex].getProductivity() * (1 + multIncrease) ),
                    factory.getTruckProductivity()) - totalProd
//            println("ÑLKASD $multIncrease JFASDÑL: " + (totalProd - factory.generators[generatorIndex].getProductivity()) + factory.generators[generatorIndex].getProductivity() * (1 + multIncrease)
//                    + "\n" + "FFFFFFFFFFFFFFFFFFFFFFFFFF: " + totalProd)
        }
        println("GEN IMPROV: ${improvement * gs.cashPerRubberDuck}")

        return (improvement * gs.cashPerRubberDuck)
    }

    override fun forSimulationGetDescription(): String {
        return description
    }
}

class TruckMultResearch(gameScreen: GameScreen, price: Double,
                      val truckIndex: Int, val multIncrease: Double, ducksNeeded: Int = -1)
    : Research(gameScreen, price, ducksNeeded = ducksNeeded) {
    init {
        description = "Increase productivity of each ${Factory.getGeneratorName(truckIndex, true)} " +
                "by ${gameScreen.formatNumber(multIncrease)} ducks shipped per second"
        buyAction = {gameScreen.truckMult[truckIndex] += multIncrease}
    }

    override fun forSimulationGetImprovementMoneyPerSecond(gs: GameScreen): Double {
        var improvement = 0.0
        for (factory in gs.factories) {
            if (factory is GameScreen.BuyableFactory) continue
            val difference = factory.getGeneratorProductivity() - factory.getTruckProductivity()
            val impr = ((factory.getTruckProductivity() - factory.trucks[truckIndex].getProductivity())
                    + factory.trucks[truckIndex].getProductivity() * (1 + multIncrease)) - factory.getTruckProductivity()
            improvement += Math.min(difference, impr)
            println(improvement)
        }

        return (improvement * gs.cashPerRubberDuck)
    }

    override fun forSimulationGetDescription(): String {
        return description
    }
}

class DuckPerTouchPercentOfProdResearch(gameScreen: GameScreen, price: Double, val increase: Double, ducksNeeded: Int = -1)
    : Research(gameScreen, price, ducksNeeded = ducksNeeded) {
    init {
        description = "Increase ducks produced per touch by ${gameScreen.formatNumber(increase * 100.0)}% of total production"
        buyAction = {gameScreen.ducksPerTouchPercentOfProduction += increase}
    }

    override fun forSimulationGetImprovementMoneyPerSecond(gs: GameScreen): Double {
        return (increase * gs.statDucksPerSecond * gs.cashPerRubberDuck)
    }

    override fun forSimulationGetDescription(): String {
        return description
    }
}

interface Buyable {
    fun buyOne()
    fun getPrice(): Double

    fun forSimulationGetImprovementMoneyPerSecond(gs: GameScreen): Double
    fun forSimulationGetDescription(): String
}