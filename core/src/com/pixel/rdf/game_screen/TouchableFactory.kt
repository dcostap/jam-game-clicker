package com.pixel.rdf.game_screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Array
import com.dcostap.engine_2d.engine.ui.utils.ExtImage
import com.dcostap.engine_2d.engine.ui.utils.ExtLabel
import com.dcostap.engine_2d.engine.utils.Timer
import com.dcostap.engine_2d.engine.utils.Utils
import com.dcostap.engine_2d.engine.utils.percentOfAppHeight
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth

/**
 * Created by Darius on 10/04/2018.
 */
class TouchableFactory(mainUI: MainUI, val onScreenFactories: OnScreenFactories, val factory: Factory, var position: Position)
    : Group() {
    enum class Position {
        left, right, center
    }
    val isBuyableFactory = factory is GameScreen.BuyableFactory

    val mainUI = onScreenFactories.mainUI
    val assets = mainUI.assets
    val gameScreen = mainUI.gameScreen
    val uiStage = gameScreen.stage

    val region = if (isBuyableFactory) mainUI.buyableFactoryImage else mainUI.factoryImage

    var cancelInteraction = false
    var cancelTimer = Timer(onScreenFactories.animationDuration)

    val debugActor = ExtLabel("", assets.fontNotOutline.font_medium, Color.RED)

    val listener = object : InputListener() {
        override fun touchUp(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int) {
            if (cancelInteraction) return
            released()
            //println("release")
        }

        override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
            if (cancelInteraction) return false
            pressed()
            //println("press")
            return true
        }
    }

    lateinit var originalPosition: Vector2
    val unsoldCounterLabel = ExtLabel("", assets.fontNotOutline.font_medium, Color.WHITE)
    val imageFactory = ExtImage(region)
    val fallingDucks = Array<FallingDuck>()

    val maximumVisibleDucks = 20

    var labelOut = false
    var labelIn = false

    fun shakeFactory() {
        imageFactory.addAction(com.pixel.rdf.Utils.actionShake(percentOfAppWidth(1.34f)))
    }

    fun shakeAndMakeUnsolLabelRed(): Action {
        return Actions.parallel(
                com.pixel.rdf.Utils.actionShake(percentOfAppWidth(1f), unsoldCounterLabel.x, unsoldCounterLabel.y),
                Actions.sequence(Actions.color(Color.RED, 0.18f, Interpolation.pow3),
                        Actions.delay(0.6f), Actions.color(Color.BLACK, 0.32f, Interpolation.pow3)))
    }

    init {
//        addActor(debugActor)
//        debugActor.moveBy(-50f, -33f)
        imageFactory.touchable = Touchable.enabled
        imageFactory.addListener(listener)
        addActor(imageFactory)

        if (!isBuyableFactory) {
            gameScreen.stage.addActor(unsoldCounterLabel)
//            unsoldCounterLabel.color.a = 0f
            unsoldCounterLabel.x = onScreenFactories.getFactX(Position.center) + onScreenFactories.getImageWidth(Position.center) / 1.5f
            unsoldCounterLabel.y = percentOfAppHeight(100f) - mainUI.topPartHeight / 1.1f
            unsoldCounterLabel.setOrigin(Align.center)
            unsoldCounterLabel.color = Color.BLACK
            val parentFactory = this
            unsoldCounterLabel.updateFunction = {
                if (!labelOut && parentFactory.factory.unsoldDucks.toInt() == parentFactory.factory.unsoldLimit.toInt() && !unsoldCounterLabel.hasActions()
                        && parentFactory.factory.getGeneratorProductivity() != 0.0) // ignore shaking when == to limit but no ducks are produced
                {
                    shakeFactory()
                    unsoldCounterLabel.addAction(shakeAndMakeUnsolLabelRed())
                }
                unsoldCounterLabel.setText("unsold: " + factory.unsoldDucks.toInt() + "/" + factory.unsoldLimit)
                if (parentFactory.position == Position.center) {
                    if (!labelIn) {
                        unsoldCounterLabel.color = Color.BLACK
                        unsoldCounterLabel.color.a = 0f
                        labelIn = true
                        labelOut = false
                        unsoldCounterLabel.clearActions()
                        unsoldCounterLabel.addAction(Actions.sequence(Actions.delay(0.35f), Actions.fadeIn(0.22f)))
                    }
                } else {
                    if (!labelOut) {
                        unsoldCounterLabel.color = Color.BLACK
                        labelIn = false
                        labelOut = true
                        unsoldCounterLabel.clearActions()
                        unsoldCounterLabel.addAction(Actions.sequence(Actions.fadeOut(0.18f), Actions.run { unsoldCounterLabel.setText("") }))
                    }
                }
            }

            addAction(Actions.sequence(Actions.delay(0.01f), Actions.run { updateFallingDucks(true) }))
        }
    }

    override fun positionChanged() {
        super.positionChanged()

        if (!this::originalPosition.isInitialized) originalPosition = Vector2(x, y)
    }

    val delayBetweenPresses = Timer()
    val rubberDucksUnsoldCheck = Timer(0.1f)

    fun spawnSellingBenefitActor(mousePosition: Boolean = false) {
        uiStage.addActor(PopUpFadingActor(true, this).also {
            it.touchable = Touchable.disabled
            it.add(ExtLabel("+ ${gameScreen.formatMoney(gameScreen.cashPerRubberDuck * getSingleTouchSellingQuantity())}",
                    assets.fontDefault.font_small, gameScreen.mainUI.moneyColor))

            fun random(): Float {
                return Utils.getRandomFloatInsideRange(0f, percentOfAppWidth(5f), true)
            }

            if (mousePosition)
                it.setPosition(Gdx.input.x + random(),
                    Gdx.graphics.height - (Gdx.input.y) + random())
            else
                it.setPosition(percentOfAppWidth(100f - 5f),
                        percentOfAppHeight(70f) - y)
        })
    }

    var killedBecausePlayerTouch = false
    var previousDucks = 0

    private fun getSingleTouchSellingQuantity(): Int {
            return (Math.min(factory.unsoldLimit / Math.min(factory.unsoldLimit, maximumVisibleDucks.toDouble()).toInt(),
                    factory.unsoldDucks).toInt())
    }

    private fun createTheFallingDuck(instaFall: Boolean = false): FallingDuck {
        val duckNumber = fallingDucks.size

        val bottomY = percentOfAppHeight(1.15f) * Math.floor(duckNumber / 2.0).toFloat() + percentOfAppWidth(5f)

        val duckClickedTimer = Timer(0.28f)
        var duckClickedTimes = 0
        val duckClickedGrow = ExponentialGrowth().also { // when clicking to sell and not releasing, each time sells more
            it.basePrice = 1.0
            it.multiplier = 1.12
        }
        val duck = FallingDuck((duckNumber) % 2, bottomY, imageFactory.height,
                imageFactory.width.toInt() + percentOfAppWidth(1.6f).toInt(), gameScreen.mainUI.duckImage, this).also {

                    if (instaFall) {
                        it.clearActions()
                        it.y = bottomY + percentOfAppHeight(1f)
                    }
                    // let touches on the duck sell ducks
                    it.updateFunction = {delta ->
                        if (!duckClickedTimer.isTimerOn) { // it was off already = not clicked action done = touch has released
                            duckClickedTimes = 0 // reset the count
                        }
                        duckClickedTimer.turnTimerOff()
                    }
                    it.clickedAction = { delta ->
                        duckClickedTimer.turnTimerOn()
                        // sell ducks on each timer tick; each consecutive time the sold quantity grows exponentially
                        if (duckClickedTimer.tick(delta) || duckClickedTimes == 0) { // first time is instant sell
                            if (factory.unsoldDucks > 0 ) {
                                factory.extraSoldDucks += Math.min(
                                        getSingleTouchSellingQuantity() * duckClickedGrow.getValue(duckClickedTimes).toInt(),
                                        factory.unsoldDucks.toInt())

                                killedBecausePlayerTouch = true
                                spawnSellingBenefitActor(true)
                                it.isTransform = true

                                // touched animation
                                if (!it.hasActions()) {
                                    it.setScale(1.05f)
                                }
                                it.addAction(Actions.sequence(Actions.scaleTo(1f, 1f, 0.07f),
                                        Actions.scaleTo(1.15f, 1.15f, 0.11f),
                                        Actions.scaleTo(1f, 1f, 0.1f), Actions.run { it.isTransform = false }))
                            }
                            duckClickedTimes++
                        }
                    }
                }

        addActorBefore(imageFactory, duck)
        return duck
    }

    val unsoldTrigger = Timer(0.1f, true, true)

    init {
//        unsoldTrigger.turnTimerOff()
//
//        var i = 0
//        while (i < 30) {
//            i++
//            act(0.1f)
//        }
//
//        unsoldTrigger.turnTimerOn()
    }

    var simulatedCounter = 0

    val cancelInteractionTimer = Timer(0.26f)

    override fun act(delta: Float) {
        super.act(delta)

        debugActor.setText("$position , $cancelInteraction")

        if (cancelInteraction && (!hasActions() || cancelInteractionTimer.tick(delta))) {
            cancelInteraction = false
        }
        if (!cancelInteraction) {
            cancelInteractionTimer.resetTimer()
        }

        if (gameScreen.gameLoopPaused) return

        if (rubberDucksUnsoldCheck.tick(delta)) {
            if (factory.unsoldDucks > 0 || fallingDucks.size > 0) {
                unsoldTrigger.tick(delta)
            } else {
                unsoldTrigger.resetTimer()
                unsoldTrigger.turnTimerOn()
            }

            previousDucks = factory.unsoldDucks.toInt()

            if (!unsoldTrigger.isTimerOn) {
                updateFallingDucks()
            } else {
                killedBecausePlayerTouch = false
            }
        }

//        if (cancelInteraction) color = Color.BLACK else color = Color.WHITE
    }

    fun updateFallingDucks(instaFall: Boolean = false) {
        val spawnObjectiveNumber = Utils.mapToRange(factory.unsoldDucks.toFloat(), 0f,
                factory.unsoldLimit.toFloat(), 0f, Math.min(maximumVisibleDucks.toFloat(), factory.unsoldLimit.toFloat())).toInt()

        while (fallingDucks.size < spawnObjectiveNumber) {
            fallingDucks.add(createTheFallingDuck(instaFall))
        }
        while (fallingDucks.size > spawnObjectiveNumber) {
            fallingDucks.pop().also {
                it.kill = true
                if (!killedBecausePlayerTouch) {
                    //spawnSellingBenefitActor()
                }
                killedBecausePlayerTouch = false
            }
        }
    }

    fun pressTrigger() {
        imageFactory.setOrigin(Align.center)
        val factPos = position
        onScreenFactories.touchedFactoryOnScreenTrigger(position)

        if (factPos != Position.center || isBuyableFactory) return
        if (factory.unsoldDucks < factory.unsoldLimit) {
            delayBetweenPresses.resetTimer()

            val parent = this
            uiStage.addActor(PopUpFadingActor(false, this).also {
                it.touchable = Touchable.disabled
                it.add(ExtLabel("+${gameScreen.ducksPerTouch} ", assets.fontDefault.font_big))
                it.add(ExtImage(gameScreen.mainUI.duckImage).also {
                    it.scaleToWidth(percentOfAppWidth(8f))
                })

                fun random(): Float {
                    return Utils.getRandomFloatInsideRange(0f, percentOfAppWidth(4f), true)
                }

                it.setPosition(parent.x + imageFactory.width / 2 + random(),
                        parent.y + imageFactory.height / 1.5f + random())
            })

//        if (gameScreen.ducksPerTouch > 3) {
//            notSoldDucks = Math.max(Utils.getRandomInteger(3), 1)
//            instaSoldDucks = gameScreen.ducksPerTouch - notSoldDucks
//        }

            factory.extraProducedDucks += gameScreen.ducksPerTouch
        }
    }

    fun pressed() {
        imageFactory.setOrigin(Align.center)
        imageFactory.clearActions()

        fun randomValue(range: Float): Float {
            return Utils.getRandomFloatInsideRange(0f, percentOfAppWidth(range), true)
        }

        if (position == Position.center) {
            if (!isBuyableFactory && factory.unsoldDucks < factory.unsoldLimit) {
                imageFactory.addAction(Actions.sequence(Actions.scaleTo(0.93f, 0.93f, 0.05f),
                        Actions.scaleTo(0.86f, 0.86f, 0.13f, Interpolation.swingOut)))

                imageFactory.addAction(Actions.sequence(Actions.rotateBy(
                        randomValue(Utils.mapToRange(delayBetweenPresses.elapsed, 0f, 0.4f, 1.1f, 0.32f)),
                        0.12f), Actions.rotateTo(0f, 0.11f)))

                val range = Utils.mapToRange(delayBetweenPresses.elapsed, 0f, 0.4f, 2f, 0.9f)
                imageFactory.addAction(Actions.moveBy(randomValue(range), randomValue(range), 0.08f))
            } else {
                shakeFactory()
                if (!isBuyableFactory && !unsoldCounterLabel.hasActions()) { // if buyable factory, don't shake the ducks label!
                    unsoldCounterLabel.addAction(shakeAndMakeUnsolLabelRed())
                }
            }
        }
        pressTrigger()
    }

    fun released() {
        imageFactory.clearActions()
        imageFactory.addAction(Actions.scaleTo(onScreenFactories.getScale(position), onScreenFactories.getScale(position), 0.25f, Interpolation.swingOut))
        imageFactory.addAction(Actions.moveTo(0f, 0f, 0.07f))
        imageFactory.addAction(Actions.rotateTo(0f, 0.12f))
    }
}