package com.pixel.rdf.game_screen

import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.dcostap.engine_2d.engine.ui.utils.ExtLabel
import com.dcostap.engine_2d.engine.utils.percentOfAppHeight
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth
import com.dcostap.engine_2d.engine.utils.whenNotNull

/**
 * Created by Darius on 19/04/2018.
 */
class OnScreenFactories(val mainUI: MainUI) : Table() {
    val assets = mainUI.assets
    val gs = mainUI.gameScreen
    val uiStage = gs.stage
    val factories = Array<TouchableFactory?>(3, {null})
    var disappearFactory: TouchableFactory? = null

    init {
        setOrigin(Align.center)
    }

    fun factoryDisappearsAction(factory: TouchableFactory): Action = Actions.sequence(
            Actions.scaleTo(0f, 0f, 0.15f, Interpolation.pow3),
            Actions.run { factory.remove() })

    override fun act(delta: Float) {
        super.act(delta)

        // check if the factories on game screen are not equal to the touchable factories (additions, removal...),
        // and if so, create the needed ones
        val currentFactIndex = gs.factories.indexOf(gs.currentFactory, true)
        if (factories[1] == null || factories[1]!!.factory !== mainUI.gameScreen.factories[currentFactIndex]) {
            factories[1]?.addAction(factoryDisappearsAction(factories[1]!!))
            factories[1] = addFactory(TouchableFactory.Position.center, gs.currentFactory, true)
        }

        if (currentFactIndex - 1 >= 0 &&
                (factories[0] == null || factories[0]!!.factory !== mainUI.gameScreen.factories[currentFactIndex - 1])) {
            factories[0]?.addAction(factoryDisappearsAction(factories[0]!!))
            factories[0] = addFactory(TouchableFactory.Position.left, gs.factories.get(currentFactIndex - 1), true)
        }

        if (currentFactIndex + 1 < gs.factories.size &&
                (factories[2] == null || factories[2]!!.factory !== mainUI.gameScreen.factories[currentFactIndex + 1])) {
            factories[2]?.addAction(factoryDisappearsAction(factories[2]!!))
            factories[2] = addFactory(TouchableFactory.Position.right, gs.factories.get(currentFactIndex + 1), true)
        }
    }

    val basePercentageFactorySize = 45f

    private fun addFactory(position: TouchableFactory.Position, factory: Factory, appearAnimation: Boolean = false): TouchableFactory {
        val fact = TouchableFactory(mainUI, this, factory, position)
        fact.imageFactory.scaleToWidth(percentOfAppWidth(basePercentageFactorySize))

        if (appearAnimation) {
            fact.setOrigin(Align.center)
            fact.setPosition(getFactX(position), getFactY(position))
            fact.isTransform = true
            fact.setScale(0f, 0f)
            fact.addAction(Actions.scaleTo(getScale(position), getScale(position), 0.3f, Interpolation.swingOut))
        }

        uiStage.addActor(fact)
        return fact
    }

    fun getFactX(position: TouchableFactory.Position): Float {
        return when (position) {
            TouchableFactory.Position.right -> percentOfAppWidth(100f) - getImageWidth(position) / 1.5f
            TouchableFactory.Position.center -> percentOfAppWidth(48f) - getImageWidth(position) / 2
            TouchableFactory.Position.left -> -percentOfAppWidth(0f) - getImageWidth(position) / 3f
        }
    }

    fun getFactY(position: TouchableFactory.Position): Float {
        return when (position) {
            TouchableFactory.Position.center -> percentOfAppHeight(100f) - mainUI.topPartHeight / 1.2f
            else -> percentOfAppHeight(100f) - mainUI.topPartHeight / 1.6f
        }
    }

    fun getScale(position: TouchableFactory.Position): Float {
        return when (position) {
            TouchableFactory.Position.center -> 1f
            else -> 0.38f
        }
    }

    fun getImageWidth(position: TouchableFactory.Position): Float {
        return percentOfAppWidth(basePercentageFactorySize) * getScale(position)
    }
    fun getImageHeight(position: TouchableFactory.Position): Float {
        return percentOfAppHeight(basePercentageFactorySize) * getScale(position)
    }

    val animationDuration = 0.6f

    fun touchedFactoryOnScreenTrigger(position: TouchableFactory.Position) {
        if (position == TouchableFactory.Position.center) return // touched center factory = normal

        // touched another factory = sliding animation
        for (factory in factories) {
            factory.whenNotNull {
                it.clearActions()
                it.imageFactory.clearActions()
                it.setOrigin(Align.center)
                it.imageFactory.setOrigin(Align.center)
                it.cancelInteraction = true
            }
        }

        if (position == TouchableFactory.Position.right) {
            disappearFactory = factories.first()
            val index = gs.factories.indexOf(factories.last()!!.factory)
            gs.changeCurrentFactory(gs.factories[index])

            factories[0] = factories[1]
            factories[1] = factories[2]

            factories[2] = (if (index < gs.factories.size - 1) addFactory(TouchableFactory.Position.right, gs.factories[index + 1]) else null)
            factories[2].whenNotNull {
                it.cancelInteraction = true
                it.setPosition(percentOfAppWidth(100f) + percentOfAppWidth(15f), getFactY(it.position))
                it.setOrigin(Align.center)
                it.setScale(0f)
                it.addAction(Actions.parallel(Actions.scaleTo(getScale(it.position), getScale(it.position), animationDuration, Interpolation.pow4Out),
                        Actions.moveTo(getFactX(it.position), getFactY(it.position), animationDuration, Interpolation.pow4Out)))
            }
            factories[1].whenNotNull {
                it.cancelInteraction = true
                it.position = TouchableFactory.Position.center
                it.setOrigin(Align.center)
                it.addAction(Actions.parallel(Actions.scaleTo(getScale(it.position), getScale(it.position), animationDuration, Interpolation.pow4Out),
                        Actions.moveTo(getFactX(it.position), getFactY(it.position), animationDuration, Interpolation.pow4Out)))
            }
            disappearFactory.whenNotNull {
                it.cancelInteraction = true
                it.setOrigin(Align.center)
                it.addAction(Actions.sequence(Actions.parallel(Actions.scaleTo(0f, 0f, animationDuration, Interpolation.pow4Out),
                        Actions.moveTo(-percentOfAppWidth(25f), getFactY(it.position), animationDuration, Interpolation.pow4Out)),
                        Actions.run {
                    it.remove()
                }))
            }
            factories[0].whenNotNull {
                it.cancelInteraction = true
                it.position = TouchableFactory.Position.left
                it.setOrigin(Align.center)
                it.addAction(Actions.parallel(Actions.scaleTo(getScale(it.position), getScale(it.position), animationDuration, Interpolation.pow4Out),
                        Actions.moveTo(getFactX(it.position), getFactY(it.position), animationDuration, Interpolation.pow4Out)))
            }
        }

        if (position == TouchableFactory.Position.left) {
            disappearFactory = factories.last()
            val index = gs.factories.indexOf(factories.first()!!.factory)
            gs.changeCurrentFactory(gs.factories[index])
            factories[2] = factories[1]
            factories[1] = factories[0]

            factories[0] = (if (index > 0) addFactory(TouchableFactory.Position.left, gs.factories[index - 1]) else null)
            factories[0].whenNotNull {
                it.cancelInteraction = true
                it.setPosition(-percentOfAppWidth(15f), getFactY(it.position))
                it.setOrigin(Align.center)
                it.setScale(0f)
                it.addAction(Actions.parallel(Actions.scaleTo(getScale(it.position), getScale(it.position), animationDuration, Interpolation.pow4Out),
                        Actions.moveTo(getFactX(it.position), getFactY(it.position), animationDuration, Interpolation.pow4Out)))
            }
            factories[1].whenNotNull {
                it.cancelInteraction = true
                it.position = TouchableFactory.Position.center
                it.setOrigin(Align.center)
                it.addAction(Actions.parallel(Actions.scaleTo(getScale(it.position), getScale(it.position), animationDuration, Interpolation.pow4Out),
                        Actions.moveTo(getFactX(it.position), getFactY(it.position), animationDuration, Interpolation.pow4Out)))
            }
            disappearFactory.whenNotNull {
                it.cancelInteraction = true
                it.setOrigin(Align.center)
                it.addAction(Actions.sequence(Actions.parallel(Actions.scaleTo(0f, 0f, animationDuration, Interpolation.pow4Out),
                        Actions.moveTo(percentOfAppWidth(100f) + percentOfAppWidth(25f), getFactY(it.position), animationDuration, Interpolation.pow4Out)),
                        Actions.run {
                    it.remove()
                }))
            }
            factories[2].whenNotNull {
                it.cancelInteraction = true
                it.position = TouchableFactory.Position.right
                it.setOrigin(Align.center)
                it.addAction(Actions.parallel(Actions.scaleTo(getScale(it.position), getScale(it.position), animationDuration, Interpolation.pow4Out),
                        Actions.moveTo(getFactX(it.position), getFactY(it.position), animationDuration, Interpolation.pow4Out)))
            }
        }

        for (factory in factories) {
            factory.whenNotNull {
                it.originalPosition.set(getFactX(it.position), getFactY(it.position))
            }
        }
    }
}
