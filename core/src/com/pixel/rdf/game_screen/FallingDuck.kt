package com.pixel.rdf.game_screen

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import com.dcostap.engine_2d.engine.ui.utils.ExtImage
import com.dcostap.engine_2d.engine.utils.Utils
import com.dcostap.engine_2d.engine.utils.percentOfAppHeight
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth

/**
 * Created by Darius on 11/04/2018.
 */
class FallingDuck(val position: Int, val bottomY: Float, val topY: Float, val leftX: Int, region: TextureRegion, val factory: TouchableFactory) : Table() {
    val duck: ExtImage
    var clickedAction: (delta: Float) -> Unit = {}
    var killWhenFalling = false

    private val clickListener = ClickListener()
    init {
        setOrigin(Align.center)
        isTransform = true

         duck = ExtImage(region).also {
             it.touchable = Touchable.enabled
             it.addListener(clickListener)

            it.scaleToWidth(percentOfAppWidth(9f))
             it.color.a = 0f
            it.addAction(Actions.fadeIn(0.2f))
        }

        add(duck).fill().expand()

        val posX = leftX + if (position == 1) percentOfAppWidth(5f) else 0f
        setPosition(posX + Utils.getRandomFloatInsideRange(0.0f, percentOfAppWidth(0.3f)), topY)
        addAction(Actions.parallel(
                Actions.sequence(Actions.moveTo(x, bottomY, 0.4f, Interpolation.pow3In),
                        Actions.moveBy(0f, percentOfAppHeight(1f), 0.14f, Interpolation.bounceOut)),
                Actions.rotateBy(Utils.getRandomFloatInsideRange(5f, 12f, true),
                0.25f, Interpolation.pow5Out)))

        if (killWhenFalling)
            addAction(Actions.parallel(
                    Actions.sequence(Actions.moveTo(x, bottomY, 0.6f, Interpolation.pow3In),
                            Actions.moveBy(0f, percentOfAppHeight(1f), 0.2f, Interpolation.bounceOut)),
                    Actions.rotateBy(Utils.getRandomFloatInsideRange(5f, 12f, true),
                            0.5f, Interpolation.pow5Out),
                    Actions.sequence(Actions.delay(0.3f), Actions.run {
                        remove()
                        clearActions()
                    })))
    }

    var kill = false

    var justPressed = false

    var updateFunction: (delta: Float) -> Unit = {}

    override fun act(delta: Float) {
        super.act(delta)

        updateFunction(delta)

        if (clickListener.isPressed && factory.position == TouchableFactory.Position.center) {
            if (!justPressed) {
                justPressed = true
            }
            clickedAction(delta)
        } else {
            justPressed = false
        }

        if (hasActions()) return
        isTransform = false
        if (kill) {
            clearActions()
            isTransform = true
            kill = false
            duck.addAction(Actions.fadeOut(0.6f))
            addAction(Actions.sequence(Actions.moveTo(x, topY, 0.6f), Actions.run { remove() }))
        }
    }
}
