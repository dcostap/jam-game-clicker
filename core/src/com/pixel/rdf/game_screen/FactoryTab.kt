package com.pixel.rdf.game_screen

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Array
import com.dcostap.engine_2d.engine.ui.UIController
import com.dcostap.engine_2d.engine.ui.UICreator
import com.dcostap.engine_2d.engine.ui.utils.ExtButton
import com.dcostap.engine_2d.engine.ui.utils.ExtImage
import com.dcostap.engine_2d.engine.ui.utils.ExtLabel
import com.dcostap.engine_2d.engine.ui.utils.ExtTable
import com.dcostap.engine_2d.engine.utils.*
import com.pixel.rdf.defaultButtonPadding
import com.pixel.rdf.padLeftRight
import com.pixel.rdf.padTopBottom

/**
 * Created by Darius on 30/03/2018.
 */
class FactoryTab(val mainUI: MainUI, uiController: UIController) : UICreator(uiController) {
    var factoryTabEnable = false
    val assets = mainUI.gameScreen.game.assets
    var thisTable: ExtTable? = null

    override fun createUI() {
        thisTable = addUI()
    }

    /** creates UI but with an animation to the previous and new table; only if it was a factoryTab table */
    fun redoUI() {
        var done = false
        thisTable.whenNotNull {
            it.clearActions()
            it.isTransform = true
            it.setScale(1f)
            it.setOrigin(Align.center)
            val middleX = it.width / 2f
            val middleY = it.height / 2f
            it.addAction(Actions.sequence(Actions.scaleTo(0f, 0f, 0.22f, Interpolation.pow3In),
                    Actions.run {
                        thisTable = addUI().also {
                            it.isTransform = true
                            it.setOrigin(middleX, middleY)
                            it.setScale(0f, 0f)
                            it.addAction(Actions.scaleTo(1f, 1f, 0.22f, Interpolation.pow3Out))
                        }
                    }))
            done = true
        }

        if (done) return
        createUI()
    }

    private fun addUI(): ExtTable {
        mainUI.tabsTable.clearChildren()

        if (mainUI.gameScreen.currentFactory is GameScreen.BuyableFactory) {
            return mainUI.addBaseScrollPaneTableForTabs().also {
                it.add(CustomButton(skin).also {
                    it.add(ExtLabel("BUY FACTORY", assets.fontDefault.font_medium))
                    it.row()
                    it.add(ExtLabel(mainUI.gameScreen.formatMoney(mainUI.gameScreen.factoryCost.getPrice()), assets.fontDefault.font_medium))
                    it.addChangeListener {
                        mainUI.gameScreen.buyOneFactory()
                        it.isDisabled = true
                    }
                    it.enableCondition = {mainUI.gameScreen.money >= mainUI.gameScreen.factoryCost.getCost()}
                })
            }
        }

        return mainUI.addBaseScrollPaneTableForTabs().also {
            // GENERATORS
            val allGens = ArrayList<GameScreen.Generator>()
            allGens.addAll(mainUI.gameScreen.currentFactory.generators.toList())
            allGens.addAll(mainUI.gameScreen.currentFactory.trucks.toList())

            var arrayIndex = 0
            for (generator in allGens) {
                val isTruck = mainUI.gameScreen.currentFactory.trucks.contains(generator)

                if (isTruck && arrayIndex >= 3) {
                    // trucks header
                    arrayIndex = 0

                    it.add(mainUI.tableWithShadowRectangle().also {
                        it.add(ExtLabel("Rubber Duck Shipment", assets.fontDefault.font_big, alignment = Align.center))
                    }).padTop(percentOfAppHeight(4f))
                    it.row()
                } else if (!isTruck && arrayIndex == 0) {
                    // workers header
                    it.add(mainUI.tableWithShadowRectangle().also {
                        it.add(ExtLabel("Rubber Duck Making", assets.fontDefault.font_big, alignment = Align.center))
                    }).padTop(percentOfAppHeight(1f))
                    it.row()
                }

                val allowed = if (isTruck) mainUI.gameScreen.allowedTruck[arrayIndex] else mainUI.gameScreen.allowedGen[arrayIndex]

                if (allowed) {
                    it.add(mainUI.tableWithShadowRectangle().also {
                        it.add(Table().also {
                            it.add(Table().also { genUpTable ->
                                genUpTable.add(ExtLabel(
                                        Factory.getGeneratorName(arrayIndex, isTruck),
                                        assets.fontDefault.font_big)).left()
                                genUpTable.add(ExtLabel("", assets.fontNotOutline.font_medium, Color.BLACK).also {
                                    it.setTextUpdateFunction { "x${generator.number}" }
                                }).bottom().padLeft(percentOfAppWidth(1.4f))
                            })
                            it.row()
                            it.add(Table().also { genDownTable ->
                                genDownTable.add(ExtLabel("", assets.fontNotOutline.font_medium, Color.BLACK).also {
                                    it.setTextUpdateFunction { mainUI.gameScreen.formatNumber(generator.getProductivity()) }
                                })
                                genDownTable.add(ExtImage(mainUI.duckImage).also {
                                    it.scaleToWidth(percentOfAppWidth(5f))
                                }).padLeft(percentOfAppWidth(1.5f))
                                genDownTable.add(ExtLabel("/s", assets.fontNotOutline.font_medium, Color.BLACK))
                            }).left()
                        }).left().expandX()
                        it.add(CustomButton(skin).also {
                            it.add(ExtLabel((if (arrayIndex == 0 && !isTruck) "Hire" else "Buy") + " 1", assets.fontDefault.font_medium))
                            it.row()
                            it.add(ExtLabel("", assets.fontDefault.font_medium, mainUI.moneyColor).also {
                                it.setTextUpdateFunction { mainUI.gameScreen.formatMoney(generator.getPrice()) }
                            }).padTop(percentOfAppHeight(-1.5f))
                            it.enableCondition = { mainUI.gameScreen.money >= generator.getPrice() }
                            it.addChangeListener {
                                mainUI.gameScreen.money -= generator.getPrice()
                                generator.buyOne()
                            }
                            it.defaultButtonPadding()
                        }).right().minWidth(mainUI.minButtonWidth)

                    }).padTop(percentOfAppHeight(1.25f)).expandX().fillX()

                    it.row()
                }

                arrayIndex++
            }
            it.row()

            // upgrade of unsold limit
            val factory = mainUI.gameScreen.currentFactory
            it.add(mainUI.tableWithShadowRectangle().also {
                it.add(Table().also {
                    it.add(Table().also { genUpTable ->
                        genUpTable.add(ExtLabel("",assets.fontDefault.font_big).also {
                            it.setTextUpdateFunction {
                                "Increase unsold \nlimit to " +
                                    "${factory.unsoldLimitGrowth.getValue(factory.unsoldLimitUpgrade.getProductivity(
                                            factory.unsoldLimitUpgrade.number + 1).toInt())}"
                            }
                        }).left()
                        }).bottom().padLeft(percentOfAppWidth(1.4f))
                    })
                    it.row()
                    it.add(Table().also { genDownTable ->

                    }).left()
                it.add(CustomButton(skin).also {
                    it.add(ExtLabel("", assets.fontDefault.font_medium, mainUI.moneyColor).also {
                        it.setTextUpdateFunction { mainUI.gameScreen.formatMoney(factory.unsoldLimitUpgrade.getPrice()) }
                    }).padTop(percentOfAppHeight(-1.5f))
                    it.enableCondition = { mainUI.gameScreen.money >= factory.unsoldLimitUpgrade.getPrice() }
                    it.addChangeListener {
                        mainUI.gameScreen.money -= factory.unsoldLimitUpgrade.getPrice()
                        factory.unsoldLimitUpgrade.buyOne()
                    }
                    it.defaultButtonPadding()
                }).right().minWidth(mainUI.minButtonWidth)
            }).padTop(percentOfAppHeight(1.25f)).expandX().fillX()
        }
    }

    fun openBuildNewFactoryConfirmation() {
        mainUI.gameScreen.gameLoopPaused = true
        val darkeningImage = DrawingWidget(mainUI.assets, uiController.stage)
        darkeningImage.drawingFunction = {
            it.gameDrawer.color = Color.BLACK
            it.gameDrawer.alpha = 0.5f
            it.gameDrawer.drawRectangle(it.x, it.y, it.width, it.height, 0f, true)
            it.gameDrawer.resetColorAndAlpha()
        }

        darkeningImage.setFillParent(true)
        uiController.stage.addActor(darkeningImage)
        Utils.modifyScene2dActorToBlockInputBeneathItself(darkeningImage)

        uiController.stage.addActor(Table().also {
            it.setFillParent(true)

            val parent = it
            Table().let {
                parent.add(ScrollPane(it, skin).also {
                    it.setScrollingDisabled(true, false)
                })

                it.clip = false

                // contents
                val confirmWindow = it

                Utils.modifyScene2dActorToBlockInputBeneathItself(it)

                it.pad(percentOfAppWidth(5f))

                it.add(ExtLabel("Build new factory",
                        mainUI.assets.fontDefault.font_big)).pad(percentOfAppWidth(1f))
                it.row()
                val mult = mainUI.gameScreen.formatNumber(mainUI.gameScreen.getGlobalMultiplierByCurrentFactoryStats().toDouble())
                it.add(ExtLabel("You will start a new factory from 0, and you will never be able to " +
                        "go back to the current factory. \n\nHowever the new factory will have a global production " +
                        "multiplier of:",
                        mainUI.assets.fontNotOutline.font_small, Color.BLACK).also {
                    it.setWrap(true)
                }).width(percentOfAppWidth(80f))

                it.row()
                it.add(ExtLabel(mult + "x", mainUI.assets.fontNotOutline.font_big, Color.BLACK, alignment = Align.center).also {
                    it.setWrap(true)
                }).width(percentOfAppWidth(80f)).center()

                it.row()
                it.add(ExtLabel("This means the " +
                        "new factory will produce and sell more rubber ducks from the start!\n\nThe more rubber ducks you make in " +
                        "the current factory, the bigger the multiplier!",
                        mainUI.assets.fontNotOutline.font_small, Color.BLACK).also {
                    it.setWrap(true)
                }).padBottom(percentOfAppHeight(1f)).width(percentOfAppWidth(80f)).padTop(percentOfAppHeight(1f))

                it.row()
                it.add(Table().also {
                    it.add(CustomButton(skin).also {
                        it.add(ExtLabel("START NEW\nFACTORY!", mainUI.assets.fontDefault.font_medium))
                                .pad(percentOfAppWidth(2f))
                        it.addChangeListener {
                            uiController.stage.addActor(Table().also {
                                it.setFillParent(true)

                                val darkeningImage2 = DrawingWidget(mainUI.assets, uiController.stage)
                                darkeningImage2.drawingFunction = {
                                    it.gameDrawer.color = Color.BLACK
                                    it.gameDrawer.alpha = 0.5f
                                    it.gameDrawer.drawRectangle(it.x, it.y, it.width, it.height, 0f, true)
                                    it.gameDrawer.resetColorAndAlpha()
                                }

                                darkeningImage2.setFillParent(true)
                                uiController.stage.addActor(darkeningImage2)
                                Utils.modifyScene2dActorToBlockInputBeneathItself(darkeningImage2)

                                it.add(Window("", skin).also {
                                    it.pad(percentOfAppWidth(5f))
                                    it.isMovable = false
                                    it.clip = false

                                    val confirmConfirmWindow = it

                                    it.add(ExtLabel("Are you sure?",
                                            mainUI.assets.fontDefault.font_big).also {
                                        it.setWrap(true)
                                    }).pad(percentOfAppHeight(1f)).width(percentOfAppWidth(60f)).padTop(percentOfAppHeight(1f))

                                    it.row()
                                    it.add(ExtLabel("You will never see this factory again!",
                                            mainUI.assets.fontNotOutline.font_medium, Color.BLACK).also {
                                        it.setWrap(true)
                                    }).pad(percentOfAppHeight(1f)).width(percentOfAppWidth(60f)).padTop(percentOfAppHeight(1f))

                                    it.row()
                                    it.add(Table().also {
                                        it.defaults().width(percentOfAppWidth(30f))
                                        it.add(CustomButton(skin).also {
                                            it.add(ExtLabel("YES", mainUI.assets.fontDefault.font_medium)).pad(percentOfAppWidth(2f))
                                            it.addChangeListener {
                                                mainUI.gameScreen.resetGameWithPrestige()
                                            }
                                        }).padRight(percentOfAppWidth(10f))

                                        it.add(CustomButton(skin).also {
                                            it.add(ExtLabel("NO", mainUI.assets.fontDefault.font_big)).pad(percentOfAppWidth(2f))
                                            it.addChangeListener {
                                                mainUI.gameScreen.gameLoopPaused = false
                                                confirmWindow.remove()
                                                darkeningImage.remove()
                                                darkeningImage2.remove()
                                                confirmConfirmWindow.remove()
                                            }
                                        })
                                    }).padTop(percentOfAppHeight(5f))

                                    it.pack()
                                    // pop-up animation
                                    it.isTransform = true
                                    it.setScale(0f)
                                    it.setOrigin(Align.center)
                                    it.addAction(Actions.scaleTo(1f, 1f, 0.32f, Interpolation.pow2))
                                })
                            })
                        }
                    }).padRight(percentOfAppWidth(5f)).fillY()

                    it.add(CustomButton(skin).also {
                        it.add(ExtLabel("GO BACK!", mainUI.assets.fontDefault.font_big)).pad(percentOfAppWidth(2f))
                        it.addChangeListener {
                            mainUI.gameScreen.gameLoopPaused = false
                            confirmWindow.remove()
                            darkeningImage.remove()
                        }
                    }).fillY()

                    it.isTransform = true
                    it.setScale(0f)
                    it.addAction(Actions.sequence(Actions.delay(1.5f),
                            Actions.scaleTo(1f, 1f, 0.32f, Interpolation.pow2)))
                }).padTop(percentOfAppHeight(5f))

                it.pack()
                // pop-up animation
                it.isTransform = true
                it.setScale(0f)
                it.setOrigin(Align.center)
                it.addAction(Actions.scaleTo(1f, 1f, 0.32f, Interpolation.pow2))
            }
        })
    }

    override fun update(delta: Float) {
//        if (mainUI.gameScreen.money > 6f && !factoryTabEnable) {
//            factoryTabEnable = true
//
//            uiController.stage.addActor(Actor().also {
//                    it.addAction(Actions.sequence(Actions.delay(1.5f), Actions.run {
//                        uiController.stage.addActor(MainUI.HelperPopUp("Hire workers! Each one of them will make rubber " +
//                                "ducks for you", 12f, 0, mainUI).also {
//                            it.setPosition(percentOfAppWidth(50f), percentOfAppHeight(37f), Align.top)
//                            it.updateFunction = {
//                                it.isVisible = mainUI.currentTab == mainUI.factoryTab
//                                if (mainUI.gameScreen.workers > 0) {
//                                    it.kill()
//                                }
//                            }
//                        })
//                    }))
//            })
//        }
    }
}