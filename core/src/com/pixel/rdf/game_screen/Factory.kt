package com.pixel.rdf.game_screen

import com.badlogic.gdx.Gdx
import com.dcostap.engine_2d.engine.utils.Timer

open class Factory(val gameScreen: GameScreen, val factoryNumber: Int) {
    companion object {
        // increase of cost and production on the generators on new factories
        val newGeneratorsCostGrow = arrayOf(
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.2
                    it.growFast = 1.2
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.15
                    it.growFast = 1.15
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.08
                    it.growFast = 1.08
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.02
                    it.growFast = 1.02
                }
        )

        val newGenCostMultGrow = arrayOf(
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.2
                    it.growFast = 1.2
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.15
                    it.growFast = 1.15
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.08
                    it.growFast = 1.08
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.02
                    it.growFast = 1.02
                }
        )

        val newGeneratorsProdGrow = ExponentialGrowth().also {
            it.basePrice = 1.0
            it.multiplier = 1.35
            it.powerExpMult = 1.0
            it.growFast = 0.2
        }

        val newTrucksCostGrow = arrayOf(
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.2
                    it.growFast = 1.2
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.15
                    it.growFast = 1.15
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.08
                    it.growFast = 1.08
                }
        )

        val newTruckCostMultGrow = arrayOf(
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.2
                    it.growFast = 1.2
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.15
                    it.growFast = 1.15
                },
                ExponentialGrowth().also {
                    it.basePrice = 1.0
                    it.multiplier = 1.3
                    it.powerExpMult = 1.08
                    it.growFast = 1.08
                }
        )

        val newTrucksProdGrow = ExponentialGrowth().also {
            it.basePrice = 1.0
            it.multiplier = 1.35
            it.powerExpMult = 1.0
            it.growFast = 0.2
        }

        // UNSOLD LIMIT UPGRADE COST
        val newUnsoldLimitUpgradeCostGrow = ExponentialGrowth().also {
            it.basePrice = 1.0
            it.multiplier = 1.4
            it.powerExpMult = 1.07
            it.growFast = 0.25
        }
        // UNSOLD LIMIT UPGRADE COST MULTIPLIER
        val newUnsoldLimitUpgradeCostMultGrow = ExponentialGrowth().also {
            it.basePrice = 1.0
            it.multiplier = 1.12
            it.powerExpMult = 1.02
            it.growFast = 0.1
        }
        // UNSOLD LIMIT INITIAL VALUES GROWTH
        val newUnsoldLimitInitialValuesGrow = ExponentialGrowth().also {
            it.basePrice = 1.0
            it.multiplier = 1.75
            it.powerExpMult = 1.14
            it.growFast = 1.5
        }

        fun getGeneratorName(index: Int, isTruck: Boolean): String {
            val addition = ""//if (plural) "s" else ""

            if (!isTruck) {
                return when (index) {
                    0 -> "Worker$addition"
                    1 -> "Simple machine$addition"
                    2 -> "Complex machine$addition"
                    else -> "MISSINGNO."
                }
            } else
                return when (index) {
                0 -> "Van$addition"
                1 -> "Truck$addition"
                2 -> "Plane$addition"
                else -> "MISSINGNO."
            }
        }
    }

    val generators = Array(4, {GameScreen.Generator(gameScreen)})
    val trucks = Array(3, {GameScreen.Generator(gameScreen)})

    // unsoldLimit price growth
    val unsoldLimitUpgrade = object : GameScreen.Generator(gameScreen) {
        init {
            priceGrowth.basePrice = 120.0 * newUnsoldLimitUpgradeCostGrow.getValue(factoryNumber)
            priceGrowth.multiplier = 1.85 * newUnsoldLimitUpgradeCostMultGrow.getValue(factoryNumber)
            priceGrowth.powerExpMult = 1.05
            priceGrowth.growFast = 100.0
            prodIncrease = 1.0
            prodInitial = 1.0
        }

        override fun forSimulationGetDescription(): String {
            return "Upgrade unsold limit; cost: ${getCost()}; limit: ${unsoldLimit}"
        }

        override fun forSimulationGetImprovementMoneyPerSecond(gs: GameScreen): Double {
            val improvement: Double = unsoldLimitGrowth.getValue(getProductivity().toInt() + 1) - unsoldLimit
            // if ducks produced in the time it takes to sell ducks is bigger than the unsold Limit, there will be improvement
            // if yes, it will pick either that difference of production or, of course, the actual growth to the limit of the upgrade
            // if not, ducks produced can be stored in the time it takes to sell them so this upgrade will not be needed
            val value = (Math.min((soldTimer.timeLimit * getGeneratorProductivity()) - unsoldLimit, improvement)) * gs.cashPerRubberDuck
            println("IMPROVEMENT $improvement; value $value; unsoldLimit $unsoldLimit; ")
            return value
        }
    }
    // marks the actual number of unsold Limit; is incremented by the "productivity" of the previous upgrade
    val unsoldLimitGrowth = ExponentialGrowth().also {
        it.basePrice = 20.0 * newUnsoldLimitInitialValuesGrow.getValue(factoryNumber)
        it.multiplier = 1.68
        it.powerExpMult = 1.1
        it.growFast = 50.0 * newUnsoldLimitInitialValuesGrow.getValue(factoryNumber)
    }

    init {
        createGenerator(0, 5.0, 1.13, 0.6, 1.5, 5.0)
        createGenerator(1, 145.0, 1.14, 2.0, 20.5, 50.0)
        createGenerator(2, 3100.0, 1.14, 18.0, 1000.0, 600.0)
        createGenerator(3, 28000.0, 1.14, 95.0, 6000.0, 4000.0)

        createTruck(0, 15.0, 3.2, 25.0, 50.0, 85.0)
        createTruck(1, 1000.0, 3.1, 70.0)
        createTruck(2, 32000.0, 3.0, 200.0)
    }

    var permanentTruckProduction = 0.0//12.0

    val unsoldLimit: Double get() = unsoldLimitGrowth.getValue(unsoldLimitUpgrade.getProductivity().toInt())
    var unsoldDucks = 0.0

    fun getGeneratorProductivity(): Double {
        var sum = 0.0
        generators.forEachIndexed { index, generator ->
            sum += generator.getProductivity() * gameScreen.genMult[index]
        }
        return sum
    }

    private fun createGenerator(generatorIndex: Int, basePrice: Double, multiplier: Double, prodInitial: Double, prodIncrease: Double = prodInitial, growFast: Double = 0.0) {
        generators[generatorIndex].priceGrowth.basePrice = basePrice * newGeneratorsCostGrow[generatorIndex].getValue(factoryNumber)
        generators[generatorIndex].priceGrowth.multiplier = multiplier * newGenCostMultGrow[generatorIndex].getValue(factoryNumber)
        generators[generatorIndex].prodInitial = prodInitial
        generators[generatorIndex].prodIncrease = prodIncrease * newGeneratorsProdGrow.getValue(factoryNumber)
        generators[generatorIndex].priceGrowth.growFast = growFast
    }

    private fun createTruck(generatorIndex: Int, basePrice: Double, multiplier: Double, prodInitial: Double, prodIncrease: Double = prodInitial, growFast: Double = 0.0) {
        trucks[generatorIndex].priceGrowth.basePrice = basePrice * newTrucksCostGrow[generatorIndex].getValue(factoryNumber)
        trucks[generatorIndex].priceGrowth.multiplier = multiplier * newTruckCostMultGrow[generatorIndex].getValue(factoryNumber)
        trucks[generatorIndex].prodInitial = prodInitial
        trucks[generatorIndex].prodIncrease = prodIncrease * newTrucksProdGrow.getValue(factoryNumber)
        trucks[generatorIndex].priceGrowth.growFast = growFast
    }
//
//    fun getProductivityCappedToUnsoldLimit(): Double {
//        var prod = getGeneratorProductivity()
//        prod = Math.min(prod, getTruckProductivity() + unsoldLimit)
//        return prod
//    }
//
//    fun getCappedProductivity(): Double {
//        return Math.min(getTruckProductivity(), getGeneratorProductivity())
//    }
//
    fun getTruckProductivity(): Double {
        var sum = permanentTruckProduction
        trucks.forEachIndexed { index, truck->
            sum += truck.getProductivity() * gameScreen.truckMult[index]
        }
        return sum
    }

    var ducksProducedStep = 0.0
        private set
    var ducksSoldStep = 0.0
        private set

    var extraProducedDucks = 0
    var extraSoldDucks = 0
    val soldTimer = Timer(1.5f)

    fun update(delta: Float) {
        var totalProd = (getGeneratorProductivity() * delta) + extraProducedDucks
        var truckProd =
                if (soldTimer.tick(delta))
                    (getTruckProductivity() * soldTimer.timeLimit)
                else
                    0.0

        truckProd += extraSoldDucks
        var totalSold = truckProd

        extraSoldDucks = 0
        extraProducedDucks = 0

        if (totalProd > truckProd) {
            val overflow = totalProd - truckProd
            totalProd = truckProd
            val previousUnsold = unsoldDucks
            unsoldDucks += overflow
            if (unsoldDucks > unsoldLimit) unsoldDucks = unsoldLimit
            totalProd += unsoldDucks - previousUnsold
        }
        else if (totalProd < truckProd) {
            val underflow = truckProd - totalProd
            val previousUnsold = unsoldDucks
            unsoldDucks -= underflow
            if (unsoldDucks < 0) unsoldDucks = 0.0
            totalSold = totalProd
            totalSold += previousUnsold - unsoldDucks
        }

        ducksProducedStep = totalProd
        ducksSoldStep = totalSold

//        println("ducksSold: $ducksSoldStep")
//        if (ducksProducedStep > 0 || ducksSoldStep > 0)
//            println("" + ducksProducedStep + "; " + ducksSoldStep)
    }
}
