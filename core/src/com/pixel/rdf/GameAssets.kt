package com.pixel.rdf

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable
import com.dcostap.engine_2d.engine.Assets
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.engine_2d.engine.utils.font_loaders.FontSetNormal
import com.dcostap.engine_2d.engine.utils.font_loaders.FontSetSingle
import com.dcostap.engine_2d.engine.utils.font_loaders.FreeTypeFontLoader
import com.dcostap.engine_2d.engine.utils.percentOfAppHeight
import com.dcostap.engine_2d.engine.utils.percentOfAppWidth

/**
 * Created by Darius on 26/03/2018.
 */
class GameAssets : Assets(skinFolder = "skins") {
    val fontDefault = FontSetNormal(smallSize = 12, mediumSize = 14, bigSize = 18)
    val fontBig = FontSetSingle(25)
    val fontNotOutline = FontSetNormal(13, 15, 18)

    override fun initAssetLoading() {
        val ttpFontFolder = "fonts/true_type"
        val bitmapFontFolder = "fonts/bitmap"

        val resolutionBaseWidth = Engine.BASE_WIDTH
        addFontLoader(FreeTypeFontLoader(ttpFontFolder, "SairaCondensed-Medium.ttf",
                fontDefault, "font1", useDensityFactor = false, useResolutionFactor = true,
                baseResolutionWidth = resolutionBaseWidth,
                parameter = FreeTypeFontLoader.getGeneratorParameterFromConfig(Color.WHITE, 1.7f, Color.BLACK),
                borderSizeScalingFactorAgainstFontSize = 0f, lineHeight = -13))

        addFontLoader(FreeTypeFontLoader(ttpFontFolder, "SairaCondensed-Medium.ttf",
                fontBig, "font2", useDensityFactor = false, useResolutionFactor = true,
                baseResolutionWidth = resolutionBaseWidth,
                parameter = FreeTypeFontLoader.getGeneratorParameterFromConfig(Color.WHITE, 2.3f, Color.BLACK),
                borderSizeScalingFactorAgainstFontSize = 0f, lineHeight = 0))

        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.borderWidth = 0f
        parameter.borderColor = Color.BLACK
        parameter.color = Color.WHITE

        addFontLoader(FreeTypeFontLoader(ttpFontFolder, "SairaCondensed-Medium.ttf",
                fontNotOutline, "font2", useDensityFactor = false, useResolutionFactor = true,
                baseResolutionWidth = resolutionBaseWidth,
                parameter = parameter, borderSizeScalingFactorAgainstFontSize = 0f, lineHeight = -13))

        super.initAssetLoading()
    }

    override fun finishAssetLoading(): Boolean {
        if (super.finishAssetLoading()) {
            // scale skin's 9patches
            val scalingFactor1 = Engine.getResolutionFactor(900f)
            val scalingFactor2 = Engine.getResolutionFactor(600f)
            val drawables = arrayOf("button1", "button1_2", "button2", "button3", "transparencyBox",
                    "button1_disabled", "transparencyButton")
            for (name in drawables) {
                val scale = when (name) {
                    "button1", "button1_2", "button1_disabled" -> scalingFactor1
                    else -> scalingFactor2
                }
                scaleNinePatchDrawableV2(name, scale)
            }

            // things to do when finished loading all assets; add styles to the skin
            skin.add("default", Window.WindowStyle(fontDefault.font_small, Color.BLACK, skin.getDrawable("button3")))
            return true
        }

        return false
    }

    fun scaleNinePatchDrawableV2(drawableName: String, scale: Float) {
        val drawable = skin.getDrawable(drawableName)
        if (drawable is NinePatchDrawable) {
            drawable.patch.scale(scale, scale)
            drawable.patch = drawable.patch
        }
    }
}