package com.pixel.rdf

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.dcostap.engine_2d.engine.Assets
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.engine_2d.engine.utils.font_loaders.smart_font_generator.SmartFontGenerator
import com.pixel.rdf.game_screen.GameScreen
import java.util.*

/**
 * Created by Darius on 26/03/2018.
 */
class ThisGame : Engine() {
    companion object {
        val language = Language.SPANISH
        val DEBUG_CHEATS = true
        val DEBUG_RESEARCH_INDEX = false
        val DEBUG_INFO = false

        var SIMULATE = false

        /** Changes the static variables on Engine class. Call first when the app starts */
        fun initStaticVars() {
            Engine.PPM = 16
            Engine.BASE_WIDTH = 304
            Engine.BASE_HEIGHT = 540
            Engine.DEBUG = false
            Engine.DEBUG_COLLISION_TREE_CELLS = false
            Engine.DEBUG_COLLISION_TREE_UPDATES = false

            SmartFontGenerator.fontVersion = "1.4"
            SmartFontGenerator.alwaysRegenerateFonts = false
            SmartFontGenerator.desktopDebugGenerateFontsOnHomeFolder = true
        }

        private var versionProperties = Properties()

        var gitTagVersion = ""
        var commit = ""

        fun loadVersionProperties() {
            try {
                versionProperties.load(Gdx.files.internal("version.properties").read())
                gitTagVersion = versionProperties.getProperty("version") ?: ""
                commit = versionProperties.getProperty("commit") ?: ""
            } catch (exc: Exception) {
                println(exc.message)
            }
        }
    }

    enum class Language {
        SPANISH, ENGLISH
    }

    override fun getAssets(): Assets {
        return assets
    }

    lateinit var assets: GameAssets
        private set

    override fun create() {
        super.create()

        loadVersionProperties()

        this.setScreen(LoadingScreen())

        assets = GameAssets()
        assets = assets
        assets.initAssetLoading()
    }

    /** @param globalMultiplier if not -1, it will be the new global multiplier of the game
     * Use to restart game with prestige */
    fun restartGameScreen(globalMultiplier: Float = -1f, hasTutorials: Boolean = true) {
        this.setScreen(GameScreen(this, true, hasTutorials).also {
            if (globalMultiplier != -1f) {
                it.globalMultiplier = globalMultiplier
            }
        })
    }

    override fun render() {
        super.render()

        // wait for assets to load; you can move this to a loading screen!
        if (getScreen() is LoadingScreen && assets.finishAssetLoading()) {
            this.setScreen(GameScreen(this, false))
        }

        // debug purposes
        if (Gdx.input.isKeyPressed(Input.Keys.R))
            this.setScreen(GameScreen(this, false))
    }
}