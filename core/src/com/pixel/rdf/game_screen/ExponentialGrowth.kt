package com.pixel.rdf.game_screen

/**
 * Created by Darius on 11/04/2018.
 */
open class ExponentialGrowth {
    var basePrice = 0.0
    var multiplier = 0.0
    var smooth: Int = 0
    /** makes graph progression steeper, 0 -> flat diagonal line, 1 -> normal  */
    var powerExpMult = 1.0
    /** makes start faster, 0 -> normal, 1 -> almost diagonal line */
    var growFast = 0.0
    var initialValues = doubleArrayOf()

    open fun getValue(number: Int): Double {
        if (number + 1 <= initialValues.size) return initialValues[number]

        val exponent = number.toDouble() * powerExpMult
        var mult = Math.pow(multiplier, exponent)
        if (exponent == 0.0) mult = 0.0
        var value = basePrice + mult + number * growFast

        if (smooth != 0)
            value = Math.floor(value / smooth.toDouble()) * smooth

        return value
    }
}
