package com.pixel.rdf.game_screen

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.dcostap.engine_2d.engine.Assets
import com.dcostap.engine_2d.engine.utils.GameDrawer

/**
 * Created by Darius on 06/04/2018.
 */
class DrawingWidget(assets: Assets, stage: Stage): Table() {
    val gameDrawer = GameDrawer(stage.batch, assets, stage.viewport)
    var drawingFunction: (self: DrawingWidget) -> Unit = {}

    override fun draw(batch: Batch?, parentAlpha: Float) {
        drawingFunction(this)
        super.draw(batch, parentAlpha)
    }
}
