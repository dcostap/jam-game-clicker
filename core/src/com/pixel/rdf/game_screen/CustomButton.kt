package com.pixel.rdf.game_screen

import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.utils.Align
import com.dcostap.engine_2d.engine.ui.utils.ExtButton

/**
 * Created by Darius on 07/04/2018.
 *
 * Disable conditions must be in disableFunction and not updateFunction, to avoid bugs related to Actions in this
 * button
 */
open class CustomButton(skin: Skin, style: String = "default") : ExtButton(skin, styleName = style) {
    var function: () -> Unit = {}

    var buttonDisabled = false

    init {
        addChangeListener { function }
        isTransform = true
    }

    class DummyBoolean {
        var value = false
    }

    fun addChangeListener(newFunction: () -> Unit) {
        this.addListener(object: ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                var extraAction = false
                setOrigin(Align.center)

                if (hasActions()) {
                    clearActions()

                    // can't trigger again, wait till the last Action finishes
                    if (!enableCondition()) {
                        return
                    } else {
                        extraAction = true
                    }
                }

                function = newFunction

                if (!extraAction)
                    addAction(Actions.sequence(Actions.run(function),
                            Actions.scaleTo(1.1f, 1.1f, 0.1f, Interpolation.pow3),
                            Actions.scaleTo(1f, 1f, 0.05f, Interpolation.pow3)))
                else
                    addAction(Actions.sequence(Actions.run(function),
                            Actions.scaleTo(1f, 1f, 0.02f, Interpolation.pow3),
                            Actions.scaleTo(1.1f, 1.1f, 0.1f, Interpolation.pow3),
                            Actions.scaleTo(1f, 1f, 0.05f, Interpolation.pow3)))
            }
        })
    }

    override fun act(delta: Float) {
        super.act(delta)
    }
}